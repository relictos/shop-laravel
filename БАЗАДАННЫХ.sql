/*
Navicat MySQL Data Transfer

Source Server         : laravel hots
Source Server Version : 50617
Source Host           : 127.0.0.1:3306
Source Database       : shop

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-01-28 16:03:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cart_orders`
-- ----------------------------
DROP TABLE IF EXISTS `cart_orders`;
CREATE TABLE `cart_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `state_slug` char(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `confirm_code` int(11) DEFAULT NULL,
  `confirmed` smallint(1) NOT NULL DEFAULT '0',
  `contact_phone` varchar(20) NOT NULL,
  `user_comment` varchar(255) DEFAULT NULL,
  `manager_comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=cp1251;

-- ----------------------------
-- Records of cart_orders
-- ----------------------------
INSERT INTO `cart_orders` VALUES ('1', '3', '85000', 'succ', '2015-11-22 01:39:39', '2015-11-28 15:34:24', null, '0', '', 'Везите мне его быстрее', null);
INSERT INTO `cart_orders` VALUES ('2', '3', '7500', 'deny', '2015-11-22 02:36:24', '2015-11-28 15:34:28', '2147483647', '0', '', 'Хм', null);
INSERT INTO `cart_orders` VALUES ('13', '3', '50000', 'succ', '2015-11-23 18:33:50', '2015-11-23 18:33:50', null, '0', '+7(231) 232-1321', '312132123132', null);
INSERT INTO `cart_orders` VALUES ('14', '4', '25000', 'succ', '2015-11-26 17:00:52', '2015-11-30 09:35:28', null, '0', '+7(800) 555-3535', 'ПРОЩЕ ПОЗВОНИТЬ НАМ, ЧЕМ У КОГО-ТО ЗАНИМАТЬ!', 'Уже позвонил');
INSERT INTO `cart_orders` VALUES ('15', '4', '180000', 'succ', '2015-11-28 17:07:04', '2015-11-26 17:07:04', null, '0', '+7(234) 213-4234', '321214', null);
INSERT INTO `cart_orders` VALUES ('16', '3', '15000', 'deny', '2015-12-03 20:46:03', '2015-12-03 21:30:43', null, '0', '+7(123) 123-2132', 'd', '');
INSERT INTO `cart_orders` VALUES ('17', '3', '15000', 'deny', '2015-12-03 20:46:46', '2015-12-03 21:30:46', null, '0', '+7(123) 123-2132', 'd', '');
INSERT INTO `cart_orders` VALUES ('18', '3', '60000', 'deny', '2015-12-03 20:50:07', '2015-12-03 21:30:39', null, '0', '+7(213) 123-2131', 'dsad', '');
INSERT INTO `cart_orders` VALUES ('19', '3', '15000', 'wait', '2015-12-03 21:30:52', '2015-12-03 21:30:52', null, '0', '+7(213) 213-1231', '1232121', null);

-- ----------------------------
-- Table structure for `cart_order_goods`
-- ----------------------------
DROP TABLE IF EXISTS `cart_order_goods`;
CREATE TABLE `cart_order_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `type` enum('set','good') NOT NULL DEFAULT 'good',
  `target_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=cp1251;

-- ----------------------------
-- Records of cart_order_goods
-- ----------------------------
INSERT INTO `cart_order_goods` VALUES ('1', '1', 'good', '1', '3');
INSERT INTO `cart_order_goods` VALUES ('2', '1', 'good', '2', '1');
INSERT INTO `cart_order_goods` VALUES ('3', '1', 'set', '1', '2');
INSERT INTO `cart_order_goods` VALUES ('15', '13', 'good', '1', '5');
INSERT INTO `cart_order_goods` VALUES ('16', '14', 'set', '1', '1');
INSERT INTO `cart_order_goods` VALUES ('17', '15', 'good', '2', '20');
INSERT INTO `cart_order_goods` VALUES ('18', '15', 'set', '2', '2');
INSERT INTO `cart_order_goods` VALUES ('19', '15', 'good', '1', '1');
INSERT INTO `cart_order_goods` VALUES ('20', '17', 'good', '1', '1');
INSERT INTO `cart_order_goods` VALUES ('21', '18', 'good', '1', '1');
INSERT INTO `cart_order_goods` VALUES ('22', '18', 'good', '2', '1');
INSERT INTO `cart_order_goods` VALUES ('23', '18', 'good', '3', '1');
INSERT INTO `cart_order_goods` VALUES ('24', '18', 'good', '6', '1');
INSERT INTO `cart_order_goods` VALUES ('25', '19', 'good', '1', '1');

-- ----------------------------
-- Table structure for `cart_order_states`
-- ----------------------------
DROP TABLE IF EXISTS `cart_order_states`;
CREATE TABLE `cart_order_states` (
  `slug` char(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  `style` varchar(255) NOT NULL DEFAULT 'text-warning',
  PRIMARY KEY (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- ----------------------------
-- Records of cart_order_states
-- ----------------------------
INSERT INTO `cart_order_states` VALUES ('deny', 'Отклонен', 'text-danger');
INSERT INTO `cart_order_states` VALUES ('succ', 'Выполнен', 'text-success');
INSERT INTO `cart_order_states` VALUES ('wait', 'В обработке', 'text-warning');

-- ----------------------------
-- Table structure for `core_filters`
-- ----------------------------
DROP TABLE IF EXISTS `core_filters`;
CREATE TABLE `core_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET cp1251 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of core_filters
-- ----------------------------
INSERT INTO `core_filters` VALUES ('1', 'Первый фильтр');

-- ----------------------------
-- Table structure for `core_filter_params`
-- ----------------------------
DROP TABLE IF EXISTS `core_filter_params`;
CREATE TABLE `core_filter_params` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) NOT NULL,
  `param_id` int(11) NOT NULL,
  `type` char(11) NOT NULL,
  `values` tinytext CHARACTER SET cp1251 NOT NULL,
  `index` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of core_filter_params
-- ----------------------------
INSERT INTO `core_filter_params` VALUES ('1', '1', '1', 'diap', '100::3400', '3');
INSERT INTO `core_filter_params` VALUES ('2', '1', '3', 'diap', '100::2000', '1');
INSERT INTO `core_filter_params` VALUES ('3', '1', '4', 'select', 'Nvidia GTX::ATI Radeon', '2');

-- ----------------------------
-- Table structure for `core_filter_param_types`
-- ----------------------------
DROP TABLE IF EXISTS `core_filter_param_types`;
CREATE TABLE `core_filter_param_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) CHARACTER SET cp1251 NOT NULL,
  `name` varchar(255) CHARACTER SET cp1251 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of core_filter_param_types
-- ----------------------------
INSERT INTO `core_filter_param_types` VALUES ('1', 'diap', 'Числовой диапазон');
INSERT INTO `core_filter_param_types` VALUES ('2', 'multiselect', 'Множественный выбор');
INSERT INTO `core_filter_param_types` VALUES ('3', 'select', 'Выбор из списка');

-- ----------------------------
-- Table structure for `core_params`
-- ----------------------------
DROP TABLE IF EXISTS `core_params`;
CREATE TABLE `core_params` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `name` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `suffix` varchar(25) CHARACTER SET cp1251 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of core_params
-- ----------------------------
INSERT INTO `core_params` VALUES ('1', 'weight', 'Вес', '2', 'г.');
INSERT INTO `core_params` VALUES ('2', 'size', 'Размер', '1', 'см.');
INSERT INTO `core_params` VALUES ('3', 'disksize', 'Объем жесткого диска', '1', 'ГБ');
INSERT INTO `core_params` VALUES ('4', 'graphic', 'Тип видеокарты', '1', '');
INSERT INTO `core_params` VALUES ('5', 'screensize', 'Разрешение экрана', '1', 'пикс.');
INSERT INTO `core_params` VALUES ('6', 'razmer-operativnoj-pamjati', 'Размер оперативной памяти', '1', 'МБ');

-- ----------------------------
-- Table structure for `core_param_types`
-- ----------------------------
DROP TABLE IF EXISTS `core_param_types`;
CREATE TABLE `core_param_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) CHARACTER SET cp1251 NOT NULL,
  `name` varchar(255) CHARACTER SET cp1251 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of core_param_types
-- ----------------------------
INSERT INTO `core_param_types` VALUES ('1', 'text', 'Строка текста');
INSERT INTO `core_param_types` VALUES ('2', 'number', 'Число');
INSERT INTO `core_param_types` VALUES ('3', 'select', 'Значение из раскрывающегося списка');
INSERT INTO `core_param_types` VALUES ('4', 'multiselect', 'Несколько значений из списка');

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2014_10_12_100000_create_password_resets_table', '1');

-- ----------------------------
-- Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
INSERT INTO `password_resets` VALUES ('relictos@yandex.ru', '15c1dc8e6d68e0c3f6d37a86ac5a004c2f36bb1b15b43826b0d10561d927636c', '2015-11-21 19:40:47');

-- ----------------------------
-- Table structure for `shop_cats`
-- ----------------------------
DROP TABLE IF EXISTS `shop_cats`;
CREATE TABLE `shop_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `parent_cat` int(11) NOT NULL DEFAULT '0',
  `filter_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `desc` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `image` varchar(255) CHARACTER SET cp1251 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of shop_cats
-- ----------------------------
INSERT INTO `shop_cats` VALUES ('1', 'noutbuki', '2', '1', 'Ноутбуки', 'Описание', 'cats/first.png');
INSERT INTO `shop_cats` VALUES ('2', 'seccat', '0', '0', 'Вторая категория', 'Описание второй категории', 'cats/sec.png');
INSERT INTO `shop_cats` VALUES ('3', 'thirdcat', '0', '0', 'Третья категория', 'Описание третьей категории', 'cats/sec.png');
INSERT INTO `shop_cats` VALUES ('4', 'kakaja-to-kategorija', '2', '0', 'Какая-то категория', 'Привет обед', '');

-- ----------------------------
-- Table structure for `shop_goods`
-- ----------------------------
DROP TABLE IF EXISTS `shop_goods`;
CREATE TABLE `shop_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `cat_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `desc` text CHARACTER SET cp1251 NOT NULL,
  `mini_desc` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `image` varchar(255) CHARACTER SET cp1251 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of shop_goods
-- ----------------------------
INSERT INTO `shop_goods` VALUES ('1', '14-noutbuk-dexp-athena-t141-chernyj', '1', '15000', '14\" Ноутбук DEXP Athena T141 черный', 'Ноутбук DEXP Athena T141 с оптимальным соотношением цены и производительности подойдёт как для домашнего, так и офисного использования. Процессор Celeron N2840 и 2 Гб оперативной памяти позволят наслаждаться просмотром любимых фильмов, общением в социальных сетях.', 'Ноутбук DEXP Athena T141 с оптимальным соотношением цены и производительности подойдёт как для домашнего, так и офисного использования. Процессор Celeron N2840 и 2 Гб оперативной памяти позволят наслаждаться просмотром любимых фильмов, общением в...', 'goods/good1.jpg', null, '2015-12-07 10:24:52');
INSERT INTO `shop_goods` VALUES ('2', 'goodtwo', '1', '15000', 'Второй товар', 'Описание второго товара', 'Мини-описание второго товара', 'goods/good2.jpg', null, null);
INSERT INTO `shop_goods` VALUES ('3', 'goodthree', '1', '17500', 'Еще Товар', 'Описание третьего товара', 'Мини-описание этого товара', 'goods/good3.jpg', null, null);
INSERT INTO `shop_goods` VALUES ('6', 'hp-250-g4', '1', '12500', 'HP 250 G4', 'экран: 15.6\"; разрешение экрана: 1366?768; процессор: Intel Core i5 5200U; частота: 2.2 ГГц (2.7 ГГц, в режиме Turbo); память: 6144 Мб, DDR3L, 1600 МГц; HDD: 500 Гб, 5400 об/мин; AMD Radeon R5 M330 — 2048 Мб; DVD-RW; WiFi; Bluetooth; HDMI; WEB-камера; Windows 8.1', 'Еще один замечательный ноутбук. Как дела? ', 'goods/hp-250-g4.jpeg', '2015-12-02 12:11:45', '2015-12-03 19:09:46');
INSERT INTO `shop_goods` VALUES ('8', 'planshet-prestigio-multipad-color-2-3g-3777-8-gb-3g-fioletovyj', '1', '5990', 'Планшет Prestigio MultiPad Color 2 3G 3777 8 Гб 3G фиолетовый', '<p>Планшет оснащен 7-дюймовым IPS-дисплеем, мощным процессором Intel Atom X3, четырехъядерным графическим адаптером Mali 450MP4 и 1 ГБ оперативной памяти. Он совмещает впечатляющую производительность от Intel со стильным дизайном: тонкий, легкий, выполненный в разных цветовых вариациях, он отвечает всем требованиям следящих за модой молодых людей.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: center;\"><iframe class=\"embed-responsive-item\" src=\"https://www.youtube.com/embed/FsIqtqoxlDo?rel=0&amp;hd=0\" width=\"640\" height=\"385\" frameborder=\"0\" allowfullscreen=\"\">&nbsp;</iframe></p>', 'Планшет оснащен 7-дюймовым IPS-дисплеем, мощным процессором Intel Atom X3', 'goods/planshet-prestigio-multipad-color-2-3g-3777-8-gb-3g-fioletovyj.jpeg', '2015-12-13 19:36:23', '2015-12-13 19:55:36');

-- ----------------------------
-- Table structure for `shop_good_params`
-- ----------------------------
DROP TABLE IF EXISTS `shop_good_params`;
CREATE TABLE `shop_good_params` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `good_id` int(11) NOT NULL,
  `param_id` int(11) NOT NULL,
  `value` varchar(255) CHARACTER SET cp1251 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of shop_good_params
-- ----------------------------
INSERT INTO `shop_good_params` VALUES ('2', '1', '2', '10x10x10');
INSERT INTO `shop_good_params` VALUES ('3', '2', '1', '1300');
INSERT INTO `shop_good_params` VALUES ('4', '3', '1', '1100');
INSERT INTO `shop_good_params` VALUES ('5', '1', '3', '500');
INSERT INTO `shop_good_params` VALUES ('6', '2', '3', '900');
INSERT INTO `shop_good_params` VALUES ('7', '3', '3', '1500');
INSERT INTO `shop_good_params` VALUES ('8', '1', '4', 'Nvidia GTX');
INSERT INTO `shop_good_params` VALUES ('9', '2', '4', 'Nvidia GTX');
INSERT INTO `shop_good_params` VALUES ('10', '3', '4', 'ATI Radeon');
INSERT INTO `shop_good_params` VALUES ('15', '6', '2', '40x30x15');
INSERT INTO `shop_good_params` VALUES ('16', '6', '1', '1200');
INSERT INTO `shop_good_params` VALUES ('17', '6', '3', '500');
INSERT INTO `shop_good_params` VALUES ('18', '1', '1', '1900');
INSERT INTO `shop_good_params` VALUES ('21', '6', '5', '1600x900');

-- ----------------------------
-- Table structure for `shop_sets`
-- ----------------------------
DROP TABLE IF EXISTS `shop_sets`;
CREATE TABLE `shop_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `mini_desc` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251;

-- ----------------------------
-- Records of shop_sets
-- ----------------------------
INSERT INTO `shop_sets` VALUES ('1', 'pervyj-naborchik', '1', '25000', 'Первый наборчик', 'sets/first.png', '<p>Описание первого набора</p>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: center;\"><iframe class=\"embed-responsive-item\" src=\"https://www.youtube.com/embed/macnF8DpQPk?rel=0&amp;hd=0\" width=\"640\" height=\"385\" frameborder=\"0\" allowfullscreen=\"\">&nbsp;</iframe></p>\r\n<p>&nbsp;</p>', 'Жалуетесь, что некому согреть вас? Возможно, мы знаем, что вам нужно. Зайдите и посмотрите!', null, '2015-12-13 20:26:34');
INSERT INTO `shop_sets` VALUES ('2', 'set2', '1', '35000', 'Второй набор', 'sets/sec.png', 'Описание второго набора', 'Мини-описание второго набора', null, null);

-- ----------------------------
-- Table structure for `shop_set_cats`
-- ----------------------------
DROP TABLE IF EXISTS `shop_set_cats`;
CREATE TABLE `shop_set_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251;

-- ----------------------------
-- Records of shop_set_cats
-- ----------------------------
INSERT INTO `shop_set_cats` VALUES ('1', 'firstcat', 'Первый раздел ', 'Описание первого раздела наборов', 'cats/sets/first.png');
INSERT INTO `shop_set_cats` VALUES ('2', 'seccat', 'Второй раздел', 'Описание второго раздела', 'cats/sets/sec.png');

-- ----------------------------
-- Table structure for `shop_set_goods`
-- ----------------------------
DROP TABLE IF EXISTS `shop_set_goods`;
CREATE TABLE `shop_set_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `set_id` int(11) NOT NULL,
  `good_id` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=cp1251;

-- ----------------------------
-- Records of shop_set_goods
-- ----------------------------
INSERT INTO `shop_set_goods` VALUES ('1', '1', '1', '3');
INSERT INTO `shop_set_goods` VALUES ('2', '1', '2', '2');
INSERT INTO `shop_set_goods` VALUES ('3', '2', '1', '1');
INSERT INTO `shop_set_goods` VALUES ('4', '2', '2', '3');

-- ----------------------------
-- Table structure for `soc_feedbacks`
-- ----------------------------
DROP TABLE IF EXISTS `soc_feedbacks`;
CREATE TABLE `soc_feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` enum('set','good') NOT NULL DEFAULT 'set',
  `target_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `text` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=cp1251;

-- ----------------------------
-- Records of soc_feedbacks
-- ----------------------------
INSERT INTO `soc_feedbacks` VALUES ('2', '1', 'set', '1', '2015-11-20 02:36:03', null, 'Первый отзыв к набору товаров лалки');
INSERT INTO `soc_feedbacks` VALUES ('4', '3', 'good', '1', '2015-11-27 14:58:11', '2015-11-27 14:58:11', 'фвыфывфыв');
INSERT INTO `soc_feedbacks` VALUES ('6', '3', 'good', '3', '2015-11-29 13:41:23', '2015-11-29 13:41:23', 'выфвфы');
INSERT INTO `soc_feedbacks` VALUES ('7', '3', 'good', '2', '2015-12-05 07:36:24', '2015-12-05 07:36:24', 'авыаывыав');

-- ----------------------------
-- Table structure for `soc_users`
-- ----------------------------
DROP TABLE IF EXISTS `soc_users`;
CREATE TABLE `soc_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `role` (`role`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of soc_users
-- ----------------------------
INSERT INTO `soc_users` VALUES ('1', 'user', 'relictos@mail.ru', '', null, '2015-11-19 03:17:53', '2015-11-19 03:17:56');
INSERT INTO `soc_users` VALUES ('3', 'admin', 'relictos@yandex.ru', '$2y$10$MiEteAT.WMyAxatY.S0oKulr//3.m5gwp8Ps5zQ06Nl9p6d419VS2', '41ePI3dIhCWfZIap8U8ursTRYWptzD8lGQfYE28BRwFRGC2olHQbqKtcz3YO', '2015-11-21 18:24:06', '2015-11-27 14:26:08');
INSERT INTO `soc_users` VALUES ('4', 'user', 'dickson@yhg.biz', '$2y$10$KTCzkrZHLNHnx1K.V8aNO.NObu6CLpHEDncouPlrAI/YuqdaOt796', 'NgcFhYqCk4CX9Rsg9Km45WwVdsL4m4kvXWvf2vcYSFLx89Q3DdLKwALeymbW', '2015-11-26 16:57:02', '2015-11-26 17:05:24');

-- ----------------------------
-- Table structure for `soc_user_data`
-- ----------------------------
DROP TABLE IF EXISTS `soc_user_data`;
CREATE TABLE `soc_user_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `fio` varchar(255) NOT NULL,
  `country` int(11) NOT NULL,
  `region` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `gender` enum('male','female') NOT NULL DEFAULT 'male',
  `phone` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251;

-- ----------------------------
-- Records of soc_user_data
-- ----------------------------
INSERT INTO `soc_user_data` VALUES ('1', '1', 'Глазырин Михаил Евгеньевич', '0', '0', '0', 'male', '', null, null);
INSERT INTO `soc_user_data` VALUES ('2', '3', 'Глазырин Михаил Евгеньевич', '0', '0', '0', 'male', '+7(982) 650-8202', null, '2015-11-27 14:19:40');
