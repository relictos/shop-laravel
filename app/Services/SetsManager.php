<?php namespace App\Services;
    
use App\Models\Sets as Sets; 
use App\Models\SetCats as Cats; 
use App\Models\Feedbacks as Feedbacks;

class SetsManager
{
    private $sets;
    private $cats;
    
    public function __construct(Sets $sets, Cats $cats, Feedbacks $feedbacks)
    {
        $this->sets = $sets;
        $this->cats = $cats;
        $this->feedbacks = $feedbacks;
    }

    public function setInfo($set_slug)
    {
        return $this->sets->whereSlug($set_slug)->first();
    }
    
    public function catInfo($cat_slug)
    {
        return $this->cats->whereSlug($cat_slug)->first();
    }
    
    public function listCats()
    {
        return $this->cats->select(array('id','slug','name','image','desc'))->get();
    }
    
    public function listSets(Cats $cat)
    {
        return $cat->sets()->select(array('id','slug','name','image','mini_desc','price'))->paginate(10);
    }
    
    public function vidgetSets($count = 5)
    {
        return $this->sets->orderByRaw('RAND()')->take($count)->select(array('id','slug','cat_id','name','image','mini_desc','price'))->with('category')->get();
    }
    
    public function addFeedback($set_slug)
    {
        $set = $this->sets->whereSlug($set_slug)->select(array('id','slug'))->first();
        $text = \Request::input('feedback');
        $user = \Auth::user();
        
        $check_feedback = $this->feedbacks->whereType('set')->whereTargetId($set->id)->whereUserId($user->id)->count();
        if($check_feedback) return false;
        
        $feedback = new Feedbacks;
        $feedback->user_id = $user->id;
        $feedback->type = 'set';
        $feedback->target_id = $set->id;
        $feedback->text = $text;
        $feedback->save();
        
        return true;
    }
}    

?>