<?php namespace App\Services;
    
use App\Models\Goods as Goods;   
use App\Models\Sets as Sets;    
use App\Models\Orders as Orders;  
use App\Models\OrderGoods as OrderGoods; 

class CartManager
{
    private $goods;
    private $sets;
    private $cart;
    
    
    public function __construct(Goods $goods, Sets $sets)
    {
        $this->goods = $goods;
        $this->sets = $sets;
        
    }
    
    //все товары корзины
    public function fullInfo()
    {
        $cart = $this->decodeCart();
        if(!$cart) return false;
        
        foreach($cart as $key => $item)
        {
            if((!$item) || (!isset($item->slug)) || (!isset($item->type)) || (!isset($item->count)))
            {
                unset($cart[$key]);
                continue;
            }
            
            switch($item->type)
            {
                case 'good':
                    $item->info = $this->goods->whereSlug($item->slug)->mini()->first();
                break;
                case 'set':
                    $item->info = $this->sets->whereSlug($item->slug)->mini()->with('goods')->first();
                break;
                default: unset($cart[$key]);
            }
            if(!$item->info) unset($cart[$key]);
        }
        
        //$this->encodeCart($cart);
        return $cart;
    }
    
    public function miniInfo()
    {
        $cart = $this->decodeCart();
        if(!$cart) return false;
        
        foreach($cart as $key => $item)
        {
            if(!$item) unset($cart[$key]);
            
            if(!isset($item->slug)) unset($cart[$key]);
            if(!isset($item->type)) unset($cart[$key]);
            if(!isset($item->count)) unset($cart[$key]);
            
            switch($item->type)
            {
                case 'good':
                    $item->info = $this->goods->whereSlug($item->slug)->select(array('id','slug','name','price'))->first();
                break;
                case 'set':
                    $item->info = $this->sets->whereSlug($item->slug)->select(array('id','slug','name','price'))->first();
                break;
                default: unset($cart[$key]);
            }
            if(!$item->info) unset($cart[$key]);
        }
        
        //$this->encodeCart($cart);
        return $cart;
    }
    
    public function isEmpty()
    {
        $cart = $this->decodeCart();
        if(count($cart) <= 0) return true;
        
        return false;
    }
    
    public function OrderSum($cart)
    {
        if(!$cart) return 0;
        
        $summ = 0;
        
        foreach($cart as $good)
        {
            $summ += $good->info->price*$good->count;
        }
        
        return $summ;
    }
    
    public function createOrder($request)
    {
        $user = $request->user();
        $cart = $this->miniInfo();
        $order_summ = $this->OrderSum($cart);
        
        $check_orders_count = Orders::whereUserId($user->id)->whereStateSlug('wait')->count();
        if($check_orders_count>=3)
        {
            return false;
        }
        
        $order = new Orders;
        $order->user_id = $user->id;
        $order->state_slug = 'wait';
        $order->price = $order_summ;
        $order->contact_phone = $request->input('phone');
        $order->user_comment = $request->input('comment');
        
        $order->save();
        
       
        foreach($cart as $good)
        {
            $ordergood = new OrderGoods;
            $ordergood->order_id = $order->id;
            $ordergood->type = $good->type;
            $ordergood->target_id = $good->info->id;
            $ordergood->count = $good->count;
            
            $ordergood->save();
        }
        
        $this->clearCart();
        return $order;
    }
    
    //получение массива из cookie
    private function decodeCart()
    {
        if(!isset($_COOKIE['cart'])) return;
        
        $cart = $_COOKIE['cart'];
        $cart = json_decode($cart);
        
        return $cart;
    }
    
    public function encodeCart($cart)
    {
        return setcookie('cart',json_encode($cart));
    }
    
    private function clearCart()
    {
        setcookie('cart','');
    }
    
}

?>
    