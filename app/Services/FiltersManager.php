<?php namespace App\Services;
    
use App\Models\Filters as Filters; 
use App\Models\Categories as Cats; 

class FiltersManager
{
    private $filter;
    private $cats;
    
    public function __construct(Filters $filters, Cats $cats)
    {
        $this->filter = $filters;
        $this->cats = $cats;
    }
    
    //Создание экземпляра фильтра
    public function generateFilter($cat_id)
    {
        $cat = $this->cats->find($cat_id)->first();
        if(!$cat->filter_id) return false;
        
        $filter = $this->filter->find($cat->filter_id)->with('params')->first();
        $filter->priceMax = $cat->goods()->max('price');
        
        if($filter->params)
            foreach($filter->params as $param)
            {
                switch($param->type)
                {
                    case 'diap':
                        $this->paramDiap($param);
                    break;
                    
                    case 'multiselect':
                        $this->paramMultiSelect($param);
                    break;
                    
                    case 'select':
                        $this->paramMultiSelect($param);
                    break;
                    
                    default:
                        continue;
                }
                
            }
        
        return $filter;
    }
    
    public function applyFilter($goods,$filter)
    {
        if(!$filter) return false;
        if(!$goods) return false;
        
        if(!$filter->params) return false;
        
        if((\Request::has('price_min'))&&(\Request::has('price_max')))
        {
            $minprice = \request::input('price_min');
            $maxprice = \request::input('price_max');
            $goods->whereBetween('price',[$minprice,$maxprice]);
        }
        
        foreach($filter->params as $param)
        {
            switch($param->type)
            {
                case 'diap':
                    $minval = \request::input($param->info->slug.'_min');
                    $maxval = \request::input($param->info->slug.'_max');
                    
                    if((\Request::has($param->info->slug.'_min'))&&(\Request::has($param->info->slug.'_max')))
                    {
                        $goods->FilterDiap($param->info->id,[$minval,$maxval]);
                    }
                break;
                
                case 'multiselect':
                    //$this->paramMultiSelect($param);
                break;
                
                case 'select':
                    $val = \request::input($param->info->slug);
                    
                    if((\Request::has($param->info->slug)) && ($val != 'all'))
                    {
                        $goods->FilterSelect($param->info->id,$val);
                    }
                break;
                
                default:
                    continue;
            }
            
        }
    }
    
    private function paramDiap($param)
    {
        $param->values = explode('::',$param->values);
        return;
    }
    private function paramMultiSelect($param)
    {
        $param->values = explode('::',$param->values);
        return;
    }
}