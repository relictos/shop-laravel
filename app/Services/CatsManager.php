<?php namespace App\Services;
    
use App\Models\Categories as Cats;   
use App\Services\FiltersManager as Filters;   

class CatsManager
{
    private $cats;
    private $filter;
    
    public function __construct(Cats $cats, Filters $filter)
    {
        $this->cats = $cats;
        $this->filter = $filter;
    }
    
    /*
        Получение виджета категорий 
    */
    public function catsList()
    {
        return $this->cats
            ->whereParentCat('0')
            ->select(array('id','slug','name','desc','image'))
        ->get();
    }
    
    /* Информация о категории */
    public function catInfo($cat_slug)
    {
        return $this->cats->whereSlug($cat_slug)->first();
    }
    
    public function getFilter(Cats $cat)
    {
        return $this->filter->generateFilter($cat->id);
    }
    
    /* Список товаров категории */
    public function listGoods(Cats $cat, $filter = null)
    {
        $goods = $cat->goods();
        if(($filter) && (\Request::get('filter') == 'on'))
            $this->filter->applyFilter($goods,$filter);
        
        return $goods->select(array('id','slug','price','name','mini_desc','image'))->paginate(10);
        //->FilterMSelect(1,[200,400])->select(array('id','slug','price','name','mini_desc','image'))->paginate(2);
    }
    
    /* Список подкатегорий категории */
    public function listSubcats(Cats $cat)
    {
        return $cat->subcats();
    }
}    

?>