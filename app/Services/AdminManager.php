<?php namespace App\Services;
    
use App\Services\Admin\OrderStat as OrderStat;
use App\Services\Admin\GoodStat as GoodStat;

use App\Models\Orders as Orders;
use App\Models\OrderGoods as OrderGoods;
use App\Models\SetGoods as SetGoods;

use App\Models\Categories as Cats;
use App\Models\Goods as Goods;

use App\Models\Sets as Sets;
use App\Models\SetCats as SetCats;

use App\Models\Feedbacks as Feedbacks;

use App\Models\Params as Params;
use App\Models\GoodParams as GoodParams;

use App\Models\FilterParams as FilterParams;

class AdminManager
{
    private $order_stat;
    private $good_stat;
    
    public function __construct(OrderStat $orderstat,GoodStat $goodstat)
    {
        $this->order_stat = $orderstat;
        $this->good_stat = $goodstat;
    }
    
    public function getOrderStat()
    {
        return $this->order_stat;
    }
    
    public function getGoodStat()
    {
        return $this->good_stat;
    }
    
    public function newOrders()
    {
        return Orders::whereStateSlug('wait')->with('goods')->with('user')->get();
    }
    
    public function oldOrders()
    {
        return Orders::where('state_slug','<>','wait')->orderBy('updated_at','desc')->with('goods')->with('user')->paginate(20);
    }
    
    public function endOrder($order_id)
    {
        $check_order = Orders::find($order_id);
        if(!$check_order) return false;
        
        $state = \request('state');
        $comment = \request('manager_comment');
        
        $check_order->state_slug = $state;
        $check_order->manager_comment = $comment;
        
        $check_order->save();
        return true;
    }
    
    public function listGoods()
    {
        if(\Request::has('cat_id'))
            $cat_id = \Request::input('cat_id');
        else $cat_id = -1;
        
        $request = \Request::input('search');
        //if(!$request) return false;
        
        $result = new Goods;
        if($cat_id >= 0)
        {
            $result = $result->whereCatId($cat_id);
        }
        
        if($request)
        {
            $request = "%".$request."%";
            $result = $result->where('name','like',$request)
            ->orWhere('desc','like',$request)
            ->orWhere('price','like',$request);
        }
        
        $goods = $result
        ->orderBy('price')
        ->select(array('id','cat_id','slug','name'))
        ->with('category')
        ->paginate(20);
        
        return $goods;
    }
    
    public function goodNameslist()
    {
        return Goods::orderBy('cat_id')->select(['id','name'])->get();
    }
    
    public function goodInfo($slug)
    {
        return Goods::whereSlug($slug)->first();
    }
    
    public function goodSubcats()
    {
        return Cats::where('parent_cat','<>',0)->get();
    }
    
        
    public function addGood()
    {
        $good = new Goods;
        
        $good->name = \request('name');
        $good->slug = \request('slug');
        $good->price = \request('price');
        $good->cat_id = \request('cat_id');
        $good->mini_desc = \request('mini_desc');
        $good->desc = \request('desc');
        
        $ext = \Request::file('image')->guessExtension();
        $filename = $good->slug.'.'.$ext;
        
        \Request::file('image')->move('img/goods',$filename);
        
        $good->image = 'goods/'.$filename;
        
        $good->save();
        return $good;
    }
    
    public function changeGood($good_slug)
    {
        $good = Goods::whereSlug($good_slug)->first();
        if(!$good) return false;
        
        if(\Request::hasFile('image'))
        {
            \File::delete('img/'.$good->image);
            
            $ext = \Request::file('image')->guessExtension();
            $filename = $good->slug.'.'.$ext;
            \Request::file('image')->move('img/goods',$filename);
            $good->image = 'goods/'.$filename;
        }
        
                
        $good->name = \request('name');
        $good->slug = \request('slug');
        $good->price = \request('price');
        $good->cat_id = \request('cat_id');
        $good->mini_desc = \request('mini_desc');
        $good->desc = \request('desc');
        
        $good->save();
        return $good;
    }
    
    public function deleteGood($good_slug)
    {
        $good = Goods::whereSlug($good_slug)->first();
        if(!$good) return false;
        
        \File::delete('img/'.$good->image);
        
        OrderGoods::whereType('good')->whereTargetId($good->id)->delete();
        SetGoods::whereGoodId($good->id)->delete();
        
        return $good->delete();
    }
    
    public function getParams($existed = [], $except = 0)
    {
        $existed_params = [];
        if($existed)
        {
            foreach($existed as $param)
            {
                if($param->param_id == $except) continue;
                array_push($existed_params,$param->param_id);
            }
        }
        $params = Params::whereNotIn('id',$existed_params)->get();
        
        return $params;
    }
    
    public function paramInfo($id)
    {
        return Params::find($id);
    }
    
    public function addParam($good_slug)
    {
        $good = Goods::whereSlug($good_slug)->first();
        if(!$good) return false;
        
        $param_id = \request('param');
        $value = \request('value');
        
        $param = new GoodParams;
        $param->good_id = $good->id;
        $param->param_id = $param_id;
        $param->value = $value;
        
        $param->save();
        return $param;
    }
    
    public function editParam($id)
    {
        $param = Params::find($id);
        if(!$param) return false;
        
        $param->name = \request('name');
        $param->slug = \request('slug');
        $param->suffix = \request('suffix');
        
        $param->save();
        return $param;
    }
    
    public function deleteParam($id)
    {
        $param = Params::find($id);
        if(!$param) return false;
        
        GoodParams::whereParamId($id)->delete();
        FilterParams::whereParamId($id)->delete();
        
        return $param->delete();
    }
    
    public function createParam()
    {
        $name = \request('name');
        $slug = \request('slug');
        $suffix = \request('suffix');
        
        $param = new Params;
        $param->name = $name; $param->slug = $slug; $param->suffix = $suffix;
        
        $param->save();
        
        return $param;
    }
    
        
    public function goodParam($id)
    {
        return GoodParams::find($id);
    }
    
    public function editGoodParam($id)
    {
        $param = GoodParams::find($id);
        
        $param_id = \request('param');
        $value = \request('value');

        $param->param_id = $param_id;
        $param->value = $value;
        
        $param->save();
        return $param;
    }
    
    public function deleteGoodParam($id)
    {
        $param = GoodParams::find($id);
        if(!$param) return false;
        
        return $param->delete();
    }
    
    public function goodcatsList()
    {
        return Cats::whereParentCat('0')
            ->select(array('id','slug','name','desc','image'))
        ->get();
    }
    
    public function goodCatInfo($slug)
    {
        return Cats::whereSlug($slug)->first();
    }
    
    public function addGoodCat()
    {
        $name = \request('name');
        $slug = \request('slug');
        $desc = \request('desc');
        
        $parent_cat = \request('parent_cat');
        if($parent_cat > 0)
        {
            $check_parent = Cats::find($parent_cat);
            if(!$check_parent) return false;   
        }
        
        $cat = new Cats;
        $cat->name = $name; $cat->slug = $slug; $cat->parent_cat = $parent_cat; $cat->desc = $desc;
        
        $cat->save();
        
        return $cat;
    }
    
    public function editGoodCat($slug)
    {
        $cat = Cats::whereSlug($slug)->first();
        if(!$cat) return false;
        
        $name = \request('name');
        $slug = \request('slug');
        $desc = \request('desc');
        
        $parent_cat = \request('parent_cat');
        if($cat->parent_cat == 0) $parent_cat = 0;
        if(($cat->parent_cat > 0) && ($parent_cat == 0)) $parent_cat = $cat->parent_cat;
                
        if($parent_cat > 0)
        {
            $check_parent = Cats::find($parent_cat);
            if(!$check_parent) return false;   
        }
        
        $cat->name = $name; $cat->slug = $slug; $cat->parent_cat = $parent_cat; $cat->desc = $desc;
        $cat->save();
        
        return $cat;
    }
    
    public function deleteGoodCat($slug)
    {
        $cat = Cats::whereSlug($slug)->first();
        if(!$cat) return false;
        
        if($cat->subcats)
        {
            foreach($cat->subcats as $scat)
            {
                $this->deleteGoodCat($scat->slug);
            }
        }
        
        Goods::whereCatId($cat->id)->update(array('cat_id' => 0));
        
        return $cat->delete();
    }
    
    public function setsList()
    {
        return Sets::paginate(20);
    }
    
    public function setCatsList()
    {
        return SetCats::get();
    }
    
    public function setCatInfo($slug)
    {
        return SetCats::whereSlug($slug)->first();
    }
    
    public function addSetCat()
    {
        $cat = new SetCats;
        $cat->name = \request('name'); $cat->slug = \request('slug'); $cat->desc = \request('desc');
        
        $cat->save();
        return $cat;
    }
    
    public function editSetCat($slug)
    {
        $cat = SetCats::whereSlug($slug)->first();
        $cat->name = \request('name'); $cat->slug = \request('slug'); $cat->desc = \request('desc');
        
        $cat->save();
        return $cat;        
    }
    
    public function deleteSetCat($slug)
    {
        $cat = SetCats::whereSlug($slug)->first();
        if(!$cat) return false;
        
        Sets::whereCatId($cat->id)->update(array('cat_id'=>0));
        
        return$cat->delete();
    }
    
    public function setInfo($slug)
    {
        return Sets::whereSlug($slug)->first();
    }
    
    public function addSet()
    {
        $set = new Sets;
        
        $set->name = \request('name');
        $set->slug = \request('slug');
        $set->price = \request('price');
        $set->cat_id = \request('cat_id');
        $set->mini_desc = \request('mini_desc');
        $set->desc = \request('desc');
        
        $ext = \Request::file('image')->guessExtension();
        $filename = $set->slug.'.'.$ext;
        
        \Request::file('image')->move('img/sets',$filename);
        
        $set->image = 'sets/'.$filename;
        
        $set->save();
        return $set;        
    }
    
    public function editSet($slug)
    {
        $set = Sets::whereSlug($slug)->first();
        if(!$set) return false;
        
        $set->slug = \request('slug');
        
        if(\Request::hasFile('image'))
        {
            \File::delete('img/'.$set->image);
            
            $ext = \Request::file('image')->guessExtension();
            $filename = $set->slug.'.'.$ext;
            \Request::file('image')->move('img/sets',$filename);
            $set->image = 'sets/'.$filename;
        }
        
        $set->name = \request('name');
        $set->price = \request('price');
        $set->cat_id = \request('cat_id');
        $set->mini_desc = \request('mini_desc');
        $set->desc = \request('desc');
        
        $set->save();
        return $set;           
    }
    
    public function deleteSet($slug)
    {
        $set = Sets::whereSlug($slug)->first();
        if(!$set) return false;
        
        OrderGoods::whereType('set')->whereTargetId($set->id)->delete();
        SetGoods::whereSetId($set->id)->delete();
        \File::delete('img/'.$set->image);
        
        return $set->delete();
    }
    
    public function setGoodInfo($id)
    {
        return SetGoods::find($id);
    }
    
    public function setAddGood($set_id)
    {
        $good = new SetGoods;
        $good->set_id = $set_id;
        $good->good_id = \request('good_id');
        $good->count = \request('count');
        
        $good->save();
        return $good;
    }
    
    public function setEditGood($id)
    {
        $good = SetGoods::find($id);
        if(!$good) return false;
        
        $good->good_id = \request('good_id');
        $good->count = \request('count');
        
        $good->save();
        return $good;
    }
    
    public function setDeleteGood($id)
    {
        $good = SetGoods::find($id);
        if(!$good) return false;
        
        return $good->delete();        
    }
    
    public function feedbackInfo($id)
    {
        return Feedbacks::find($id);
    }
    
    public function feedbackEdit($id)
    {
        $feedback = Feedbacks::find($id);
        if(!$feedback) return false;
        
        $feedback->text = \request('text');
        $feedback->save();
        
        return $feedback;
    }
    
    public function feedbackDelete($id)
    {
        $feedback = Feedbacks::find($id);
        if(!$feedback) return false;
        
        return $feedback->delete();
    }
    
    public function listFilters()
    {
        //
    }
}

?>