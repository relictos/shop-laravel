<?php namespace App\Services;
    
use App\Models\Orders as Orders;

class OrdersManager
{
    private $orders;
    
    public function __construct(Orders $orders)
    {
        $this->orders = $orders;
    }
    
    public function listOrders($userId)
    {
        return $this->orders->whereUserId($userId)->orderBy('state_slug','desc')->orderBy('updated_at','desc')->with('goods')->get();
    }
}

?>