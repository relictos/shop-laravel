<?php namespace App\Services\Admin;
    
use App\Models\Goods as Goods; 
use App\Models\Categories as Cats; 
use App\Models\Sets as Sets;
use App\Models\SetGoods as SetGoods;
use App\Models\Orders as Orders;

class GoodStat
{
    private $goods;
    
    public $goods_count = 0;
    public $cats_count = 0;
    public $sets_count = 0;
    public $goods_bought = 0;
    public $goods_insets = 0;
    
    
    public function __construct()
    {
        $this->goods_count = Goods::count();
        $this->cats_count = Cats::count();
        $this->sets_count = Sets::count();
        $this->goods_insets = SetGoods::groupBy('good_id')->count();
    }

}