<?php namespace App\Services\Admin;
    
use App\Models\Orders as Orders; 

class OrderStat
{
    private $orders;
    
    public $today = ['count' => 0,'sum' => 0];
    public $yesterday = ['count' => 0,'sum' => 0];
    public $week = ['count' => 0,'sum' => 0];
    public $month = ['count' => 0,'sum' => 0];
    public $year = ['count' => 0,'sum' => 0];
    
    public $new_count = 0;
    
    public function __construct(Orders $orders)
    {
        $this->orders = $orders;  
        $this->getToday();
        $this->getYesterday();
        $this->getWeek();
        $this->getMonth();
        $this->getYear();
        
        $this->new_count = $this->orders->whereStateSlug('wait')->count();
    }
    
    public function init()
    {
        $this->getToday();
    }
    
    public function getToday()
    {
        $this->today['count'] = $this->orders->whereStateSlug('succ')->whereRaw('created_at >= CURDATE()')->count();
        $this->today['sum'] = $this->orders->whereStateSlug('succ')->whereRaw('created_at >= CURDATE()')->sum('price');
        return $this->today;
    }
    
    public function getYesterday()
    {
        $this->yesterday['count'] = $this->orders->whereStateSlug('succ')->whereRaw('created_at >= (CURDATE()-1) AND created_at < CURDATE()')->count();
        $this->yesterday['sum'] = $this->orders->whereStateSlug('succ')->whereRaw('created_at >= (CURDATE()-1) AND created_at < CURDATE()')->sum('price');
        return $this->yesterday;
    }
    
    public function getWeek()
    {
        $this->week['count'] = $this->orders->whereStateSlug('succ')->whereRaw('created_at >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY)')->count();
        $this->week['sum'] = $this->orders->whereStateSlug('succ')->whereRaw('created_at >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY)')->sum('price');
        return $this->week;
    }
    
    public function getMonth()
    {
        $this->month['count'] = $this->orders->whereStateSlug('succ')->whereRaw('created_at >= DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH)')->count();
        $this->month['sum'] = $this->orders->whereStateSlug('succ')->whereRaw('created_at >= DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH)')->sum('price');
        return $this->month;
    }

    public function getYear()
    {
        $this->year['count'] = $this->orders->whereStateSlug('succ')->whereRaw('created_at >= DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR)')->count();
        $this->year['sum'] = $this->orders->whereStateSlug('succ')->whereRaw('created_at >= DATE_SUB(CURRENT_DATE, INTERVAL 1 YEAR)')->sum('price');
        return $this->month;
    }
    
}