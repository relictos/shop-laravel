<?php namespace App\Services;
    
use App\Models\Goods as Goods;   
use App\Models\Feedbacks as Feedbacks;

class GoodsManager
{
    private $goods;
    
    public function __construct(Goods $goods, Feedbacks $feedbacks)
    {
        $this->goods = $goods;
        $this->feedbacks = $feedbacks;
    }

    
    /* ���������� � ������ */
    public function goodInfo($good_slug)
    {
        return $this->goods->whereSlug($good_slug)->first();
    }
    
    public function vidgetGoods($count)
    {
        return $this->goods->where('cat_id','>',0)->orderByRaw('RAND()')->take($count)->select(array('id','cat_id','slug','name','image','price'))->with('category')->get();
    }
    
    public function searchgoods()
    {
        $request = \Request::input('request');
        if(!$request) return false;
        
        $request = "%".$request."%";
        
        $result = $this->goods
            ->where('name','like',$request)
            ->orWhere('desc','like',$request)
            ->orWhere('price','like',$request)
            ->orderBy('price')
        ->select(array('id','cat_id','slug','price','name','mini_desc','image'))
        ->with('category')
        ->paginate(10);
        
        return $result;
    }

    public function addFeedback($good_slug)
    {
        $good = $this->goods->whereSlug($good_slug)->select(array('id','slug'))->first();
        $text = \Request::input('feedback');
        $user = \Auth::user();
        
        $check_feedback = $this->feedbacks->whereType('good')->whereTargetId($good->id)->whereUserId($user->id)->count();
        if($check_feedback) return false;
        
        $feedback = new Feedbacks;
        $feedback->user_id = $user->id;
        $feedback->type = 'good';
        $feedback->target_id = $good->id;
        $feedback->text = $text;
        $feedback->save();
        
        return true;
    }
}    

?>