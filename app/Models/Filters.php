<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Filters extends Model {
    
    protected $table = 'core_filters';
    
    protected $fillable = ['*'];
    public $timestamps = false;
    
    public function params()
    {
        return $this->hasMany('App\Models\FilterParams','filter_id')->orderBy('index')->with('info');
    }
}