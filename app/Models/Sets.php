<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sets extends Model {
    
    protected $table = 'shop_sets';
    
    public function scopeMini($query)
    {
        return $query->select(array('id','slug','cat_id','price','name','mini_desc','image'));
    }
    
    public function goods()
    {
        return $this->hasMany('App\Models\SetGoods','set_id')->with('good');
    }
        
    public function feedbacks()
    {
        return $this->hasMany('App\Models\Feedbacks','target_id')->where('type','=','set')->orderBy('created_at','desc');
    }
    
    public function category()
    {
        return $this->belongsTo('App\Models\SetCats','cat_id')->select(array('id','slug','name'));
    }
}