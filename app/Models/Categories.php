<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model {
    
    protected $table = 'shop_cats';
    
    protected $fillable = ['name','desc','image'];
    
    public $timestamps = false;
    
    /*
        Получение подкатегорий (без иконки и описания)
    */ 
    public function scopeMini($query)
    {
        return $query->select(array('id','parent_cat','slug','name'));
    }
     
    public function subcats()
    {
        return $this->hasMany('App\Models\Categories','parent_cat')->select(array('id','slug','name'));
    }
    
    public function goodsCount()
    {
        return $this->hasMany('App\Models\Goods','cat_id')->count();
    }
    
    public function goods()
    {
        return $this->hasMany('App\Models\Goods','cat_id');
    }
    
}