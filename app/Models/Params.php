<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Params extends Model {
    
    public $timestamps = false;
    protected $table = 'core_params';
    
    public function goodscount()
    {
        return $this->hasMany('App\Models\GoodParams','param_id')->count();
    }
}