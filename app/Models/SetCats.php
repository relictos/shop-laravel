<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SetCats extends Model {
    
    protected $table = 'shop_set_cats';
    
    protected $fillable = ['name','image'];
    
    public $timestamps = false;
    
    public function sets()
    {
        return $this->hasMany('App\Models\Sets','cat_id')->select(array('id','slug','price','name','image','mini_desc'))->with('goods');
    }
    
}