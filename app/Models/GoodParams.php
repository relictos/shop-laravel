<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodParams extends Model {
    
    protected $table = 'shop_good_params';
    
    public $timestamps = false;
    
    public function param()
    {
        return $this->belongsTo('App\Models\Params','param_id')->select(array('id','name','suffix'));
    }
    
}