<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilterParams extends Model {
    
    protected $table = 'core_filter_params';
    
    protected $fillable = ['*'];
    public $timestamps = false;
    
    public function info()
    {
        return $this->hasOne('App\Models\Params','id','param_id');
    }
}