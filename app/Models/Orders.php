<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model {
    
    protected $table = 'cart_orders';
    
    protected $fillable = ['*'];
    
    public function goods()
    {
        return $this->hasMany('App\Models\OrderGoods','order_id');
    }
    
    public function state()
    {
        return $this->hasOne('App\Models\OrderStates','slug','state_slug');
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}