<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Goods extends Model {
    
    protected $table = 'shop_goods';
    
    protected $fillable = ['name','price','desc','mini_desc','image'];
    
    //добавление фильтра - диапазон (ид параметра, массив значений)
    public function scopeFilterDiap($query,$param_id,$vals)
    {
        $query->whereHas('params', function($q) use ($param_id,$vals) 
        {
            $q->where('param_id', '=', $param_id)->whereBetween(\DB::raw('CAST(value as SIGNED)'),$vals);
        
        });
    }
    
    //добавление фильтра - единичное значение
    public function scopeFilterSelect($query,$param_id,$val)
    {
        $query->whereHas('params', function($q) use ($param_id,$val)
        {
            $q->where('param_id', '=', $param_id)->where('value','=',$val);
        
        });
    }
    
    //добавление фильтра - одно из списка значений
    public function scopeFilterMSelect($query,$param_id,$vals)
    {
        $query->whereHas('params', function($q) use ($param_id,$vals) 
        {
            $q->where('param_id', '=', $param_id)->whereIn('value',$vals);
        
        });
    }
    
    public function scopeMini($query)
    {
        return $query->select(array('id','slug','cat_id','price','name','mini_desc','image'));
    }
    
    public function category()
    {
        return $this->belongsTo('App\Models\Categories','cat_id')->select(array('id','slug','name'));
    }
    
    public function feedbacks()
    {
        return $this->hasMany('App\Models\Feedbacks','target_id')->where('type','=','good')->orderBy('created_at','desc');
    }
    
    public function params()
    {
        return $this->hasMany('App\Models\GoodParams','good_id')->with('param');
    }
    
}