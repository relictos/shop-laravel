<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SetGoods extends Model {
    
    protected $table = 'shop_set_goods';
    public $timestamps = false;
    
    public function good()
    {
        return $this->belongsTo('App\Models\Goods','good_id')->select(array('id','cat_id','slug','name','price','image','mini_desc'));
    }
}