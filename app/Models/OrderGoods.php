<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderGoods extends Model {
    
    protected $table = 'cart_order_goods';
    
    protected $fillable = ['*'];
    public $timestamps = false;
    
    public function info()
    {
        if($this->type == 'set')
            return $this->hasOne('App\Models\Sets','id','target_id');
        else
            return $this->hasOne('App\Models\Goods','id','target_id');
    }
}