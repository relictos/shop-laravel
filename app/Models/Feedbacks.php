<?php   namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedbacks extends Model {
    
    protected $table = 'soc_feedbacks';
    

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id')->select(array('id'));
    }
    
    public function info()
    {
        if($this->type == 'set')
            return $this->hasOne('App\Models\Sets','id','target_id');
        else
            return $this->hasOne('App\Models\Goods','id','target_id');
    }
}