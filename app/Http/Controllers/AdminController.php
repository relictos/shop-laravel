<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\AdminManager as Admin;

class AdminController extends Controller
{
    private $admin_manager;
    private $user;
    
    public function __construct(Admin $admin)
    {
        $this->middleware('auth');
        $this->middleware('admin');
        
        $this->admin_manager = $admin;
        
        $this->user = \Auth::user();
        
        array_push($GLOBALS['breadcrumb'],(['/','Главная']));
        array_push($GLOBALS['breadcrumb'],(['shopmanager','Панель администратора']));
        
        \View::share('selmenu','main');
    }
    
    public function getIndex()
    {
        $order_stat = $this->admin_manager->getOrderStat();
        $good_stat = $this->admin_manager->getGoodStat();
        
        return view('shop.admin.main_page',['order_stat'=>$order_stat,'good_stat'=>$good_stat]);
    }
    
    public function getOrders()
    {
        $orders = $this->admin_manager->newOrders();
        
        array_push($GLOBALS['breadcrumb'],(['#','Новые заказы']));
        \View::share('selmenu','orders');
        
        return view('shop.admin.new_orders',['orders'=>$orders]);
    }
    
    public function postOrders(Request $request,$order_id)
    {
        $this->validate($request, [
            'state' => 'required|alpha_num|in:succ,deny',
            'manager_comment' => 'max:255',
        ]);
        
        $result = $this->admin_manager->endOrder($order_id);
        if(!$result)
            return Redirect::back()->withErrors(['Заказ не найден']);
        else
            return redirect('shopmanager/orders')->with('messages',['Заказ успешно изменен']); 
    }
    
    public function getOrdersold()
    {
        $orders = $this->admin_manager->oldOrders();
        
        array_push($GLOBALS['breadcrumb'],(['#','История заказов']));
        \View::share('selmenu','orders');
        
        return view('shop.admin.old_orders',['orders'=>$orders]);        
    }
    
    public function getGoods()
    {
        array_push($GLOBALS['breadcrumb'],(['#','Товары']));
        \View::share('selmenu','goods');
        
        $cats = $this->admin_manager->goodSubcats();
        $goods = $this->admin_manager->listGoods();
        
        return view('shop.admin.goods',['goods'=>$goods,'cats'=>$cats]);
    }
    
    public function getGood($good_slug)
    {
        $good = $this->admin_manager->goodInfo($good_slug);
        if(!$good) abort(404);
        
        \View::share('selmenu','goods');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/goods','Товары']));
        array_push($GLOBALS['breadcrumb'],(['#',$good->name]));
        
        return view('shop.admin.good_info',['good'=>$good]);        
    }
    
    public function getAddgood()
    {
        \View::share('selmenu','goods');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/goods','Товары']));
        array_push($GLOBALS['breadcrumb'],(['#','Добавить товар']));
        
        $cats = $this->admin_manager->goodSubcats();
        
        return view('shop.admin.goods_add',['cats'=>$cats]);
    }
    
    public function postAddgood(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|alpha_dash|max:255|unique:shop_goods,slug',
            'cat_id' => 'required|numeric|exists:shop_cats,id',
            'price' => 'required|numeric|min:0',
            'image' => 'required|image|max:500',
            'mini_desc' => 'required|string|max:255',
            'desc' => 'required|string'
        ]);    
        
        $result = $this->admin_manager->addGood();
        if(!$result) return Redirect::back()->withErrors(['Ошибка при добавлении товара']);
        
        return \Redirect::to('shopmanager/good/'.$result->slug)->with('messages',['Товар успешно добавлен']);    
    }
    
    public function getEditgood($good_slug)
    {
        
        $good = $this->admin_manager->goodInfo($good_slug);
        if(!$good) abort(404);
        
        \View::share('selmenu','goods');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/goods','Товары']));
        array_push($GLOBALS['breadcrumb'],(['#','Изменить товар']));
        
        $cats = $this->admin_manager->goodSubcats();
        
        return view('shop.admin.goods_edit',['cats'=>$cats,'good'=>$good]);
    }
    
    public function postEditgood(Request $request, $good_slug)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|alpha_dash|max:255|unique:shop_goods,slug,'.$good_slug.',slug',
            'cat_id' => 'required|numeric|exists:shop_cats,id',
            'price' => 'required|numeric|min:0',
            'image' => 'image|max:500',
            'mini_desc' => 'required|string|max:255',
            'desc' => 'required|string'
        ]);  
        
        $result = $this->admin_manager->changeGood($good_slug);
        if(!$result) return Redirect::back()->withErrors();
        
        return \Redirect::to('shopmanager/good/'.$result->slug)->with('messages',['Товар успешно изменен']);          
    }
    
    public function postDeletegood(Request $request, $good_slug)
    {
        $result = $this->admin_manager->deleteGood($good_slug);
        if(!$result) return Redirect::back()->withErrors(['Не удалось удалить товар']);
        
        return \Redirect::to('shopmanager/goods')->with('messages',['Товар успешно удален']);
    }
    
    public function getAddgoodparam($good_slug)
    {
        $good = $this->admin_manager->goodInfo($good_slug);
        if(!$good) abort(404);
        
        \View::share('selmenu','goods');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/goods','Товары']));
        array_push($GLOBALS['breadcrumb'],(['shopmanager/good/'.$good_slug,$good->name]));
        array_push($GLOBALS['breadcrumb'],(['#','Добавить свойство']));
        
        $params = $this->admin_manager->getParams($good->params);
        
        return view('shop.admin.good_addparam',['params'=>$params,'good'=>$good]);
    }
    
    public function postAddgoodparam($good_slug, Request $request)
    {
        $this->validate($request, [
            'param' => 'required|numeric|exists:core_params,id',
            'value' => 'required|max:255',
        ]);    
        
        $result = $this->admin_manager->addParam($good_slug);
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при добавлении характеристики']);
        
        return \Redirect::to('shopmanager/good/'.$good_slug)->with('messages',['Характеристика успешно добавлена']);     
    }
    
    public function getEditgoodparam($good_slug,$id)
    {
        $good = $this->admin_manager->goodInfo($good_slug);
        if(!$good) abort(404);
        
        $param = $this->admin_manager->goodParam($id);
        if(!$param) abort(404);
        
        $params = $this->admin_manager->getParams($good->params);
        
        \View::share('selmenu','goods');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/goods','Товары']));
        array_push($GLOBALS['breadcrumb'],(['shopmanager/good/'.$good_slug,$good->name]));
        array_push($GLOBALS['breadcrumb'],(['#','Изменить свойство']));
        
        $params = $this->admin_manager->getParams($good->params,$param->param_id);
        
        return view('shop.admin.good_editparam',['params'=>$params,'good'=>$good,'param'=>$param]);        
    }
    
    public function postEditgoodparam($good_slug, $id, Request $request)
    {
        $this->validate($request, [
            'param' => 'required|numeric|exists:core_params,id',
            'value' => 'required|max:255',
        ]);    
        
        $result = $this->admin_manager->editGoodParam($id);
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при изменении характеристики']);
        
        return \Redirect::to('shopmanager/good/'.$good_slug)->with('messages',['Характеристика успешно изменена']);     
    }
    
    public function postDeletegoodparam($good_slug, $id, Request $request)
    {
        $result = $this->admin_manager->deleteGoodParam($id);
        if(!$result) return Redirect::back()->withErrors(['Не удалось удалить характеристику']);
        
        return \Redirect::back()->with('messages',['Характеристика успешно удалена']);
    }
    
    public function getParams()
    {
        $params = $this->admin_manager->getParams([]);
        
        \View::share('selmenu','params');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/params','Характеристики']));
        
        return view('shop.admin.params_list',['params'=>$params]);
    }
        
    public function postCreateparam(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|alpha_dash|max:255|unique:core_params,slug',
            'suffix' => 'string|max:25',
        ]);
        
        $result = $this->admin_manager->createParam();
        
        if(!$result) \Redirect::back()->withErrors(['Ошибка при создании характеристики']);
        
        return \Redirect::back()->with('messages',['Характеристика создана. Теперь вы можете выбрать ее из списка']);
    }
    
    public function getEditparam($id)
    {
        $param = $this->admin_manager->paramInfo($id);
        if(!$param) abort(404);
        
        \View::share('selmenu','params');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/params','Характеристики']));
        array_push($GLOBALS['breadcrumb'],(['#','Редактировать']));
        
        return view('shop.admin.param_edit',['param'=>$param]);
    }
    
    public function postEditparam($id,Request $request)
    {
        $param = $this->admin_manager->paramInfo($id);
        if(!$param) abort(404);
        
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|alpha_dash|max:255|unique:core_params,slug,'.$param->slug.',slug',
            'suffix' => 'string|max:25',
        ]);
        
        $result = $this->admin_manager->editParam($id);
        
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при изменении характеристики']);
        
        return \Redirect::to('shopmanager/params')->with('messages',['Характеристика изменена']);
    }
    
    public function postDeleteparam($id, Request $request)
    {
        $result = $this->admin_manager->deleteParam($id);
        if(!$result) return Redirect::back()->withErrors(['Не удалось удалить характеристику']);
        
        return \Redirect::back()->with('messages',['Характеристика успешно удалена']);
    }
    
    public function getCategories()
    {
        
        \View::share('selmenu','cats');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/categories','Категории товаров']));

        $cats = $this->admin_manager->goodcatsList();
        
        return view('shop.admin.cats_list',['cats'=>$cats]);
    }
    
    public function getAddcat()
    {
        
        \View::share('selmenu','cats');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/categories','Категории товаров']));
        array_push($GLOBALS['breadcrumb'],(['#','Добавить категорию']));

        $cats = $this->admin_manager->goodcatsList();
        
        return view('shop.admin.cat_add',['cats'=>$cats]);        
    }
    
    public function postAddcat(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|alpha_dash|max:255|unique:shop_cats,slug',
            'parent_cat' => 'required|numeric',
            'desc' => 'required|string|max:255',
        ]);    
        
        $result = $this->admin_manager->addGoodCat();
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при создании категории']);
        
        return \Redirect::to('shopmanager/categories')->with('messages',['Категория успешно создана']);            
    }
    
    public function getEditcat($cat_slug)
    {
        $cat_info = $this->admin_manager->goodCatInfo($cat_slug);
        if(!$cat_info) abort(404);
        
        \View::share('selmenu','cats');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/categories','Категории товаров']));
        array_push($GLOBALS['breadcrumb'],(['#','Изменить категорию']));

        $cats = $this->admin_manager->goodcatsList();
        
        return view('shop.admin.cat_edit',['cat'=>$cat_info,'cats'=>$cats]);          
    }
    
    public function postEditcat(Request $request, $cat_slug)
    {
        $cat_info = $this->admin_manager->goodCatInfo($cat_slug);
        if(!$cat_info) abort(404);
        
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|alpha_dash|max:255|unique:shop_cats,slug,'.$cat_info->slug.',slug',
            'parent_cat' => 'required|numeric',
            'desc' => 'required|string|max:255',
        ]);    
        
        $result = $this->admin_manager->editGoodCat($cat_slug);
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при изменении категории']);
        
        return \Redirect::to('shopmanager/categories')->with('messages',['Категория успешно изменена']);            
    }
    
    public function postDelcat(Request $request, $cat_slug)
    {
        $result = $this->admin_manager->deleteGoodCat($cat_slug);
        if(!$result) return Redirect::back()->withErrors(['Не удалось удалить категорию']);
        
        return \Redirect::back()->with('messages',['Категория успешно удалена']);        
    }
    
    public function getSets()
    {
        \View::share('selmenu','sets');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/sets','Наборы товаров']));

        $sets = $this->admin_manager->setsList();
        $cats = $this->admin_manager->setCatsList();
        
        return view('shop.admin.sets_list',['cats'=>$cats,'sets'=>$sets]);        
    }
    
    public function getAddsetcat()
    {
        \View::share('selmenu','sets');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/sets','Наборы товаров']));
        array_push($GLOBALS['breadcrumb'],(['#','Добавить категорию наборов']));

        return view('shop.admin.setcat_add');          
    }
    
    public function postAddsetcat(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|alpha_dash|max:255|unique:shop_set_cats,slug',
            'desc' => 'required|string|max:255',
        ]);  
        
        $result = $this->admin_manager->addSetCat();
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при создании категории']);
        
        return \Redirect::to('shopmanager/sets')->with('messages',['Категория успешно добавлена']);        
    }
    
    public function getEditsetcat($slug)
    {
        $cat = $this->admin_manager->setCatInfo($slug);
        if(!$cat) abort(404);
        
        \View::share('selmenu','sets');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/sets','Наборы товаров']));
        array_push($GLOBALS['breadcrumb'],(['#','Изменить категорию наборов']));

        return view('shop.admin.setcat_edit',['cat'=>$cat]);         
    }
    
    public function postEditsetcat(Request $request, $slug)
    {
        $cat = $this->admin_manager->setCatInfo($slug);
        if(!$cat) abort(404);
        
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|alpha_dash|max:255|unique:shop_set_cats,slug,'.$cat->slug.',slug',
            'desc' => 'required|string|max:255',
        ]);  
        
        $result = $this->admin_manager->editSetCat($slug);
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при изменении категории']);
        
        return \Redirect::to('shopmanager/sets')->with('messages',['Категория успешно изменена']);        
    }
    
    public function postDelsetcat($slug)
    {
        $result = $this->admin_manager->deleteSetCat($slug);
        if(!$result) return Redirect::back()->withErrors(['Не удалось удалить категорию']);
        
        return \Redirect::back()->with('messages',['Категория успешно удалена']);             
    }
    
    public function getAddset()
    {
        $cats = $this->admin_manager->setCatsList();
        
        \View::share('selmenu','sets');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/sets','Наборы товаров']));
        array_push($GLOBALS['breadcrumb'],(['#','Добавить набор товаров']));

        return view('shop.admin.set_add',['cats'=>$cats]);          
    }
    
    public function postAddset(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|alpha_dash|max:255|unique:shop_sets,slug',
            'cat_id' => 'required|numeric|exists:shop_set_cats,id',
            'price' => 'required|numeric|min:0',
            'image' => 'required|image|max:500',
            'mini_desc' => 'required|string|max:255',
            'desc' => 'required|string'
        ]);  
        
        $result = $this->admin_manager->addSet();
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при создании набора товаров']);
        
        return \Redirect::to('shopmanager/set/'.$result->slug)->with('messages',['Набор успешно создан']);
    }
    
    public function getEditset($slug)
    {
        $set = $this->admin_manager->setInfo($slug);
        if(!$set) abort(404);
        
        \View::share('selmenu','sets');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/sets','Наборы товаров']));
        array_push($GLOBALS['breadcrumb'],(['shopmanager/set/'.$set->slug,$set->name]));
        array_push($GLOBALS['breadcrumb'],(['#','Изменить набор']));
        
        $cats = $this->admin_manager->setCatsList();
        
        return view('shop.admin.set_edit',['cats'=>$cats,'set'=>$set]);        
    }
    
    public function postEditset(Request $request, $slug)
    {
        $set = $this->admin_manager->setInfo($slug);
        if(!$set) abort(404);
        
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'slug' => 'required|alpha_dash|max:255|unique:shop_sets,slug,'.$set->slug.',slug',
            'cat_id' => 'required|numeric|exists:shop_set_cats,id',
            'price' => 'required|numeric|min:0',
            'image' => 'image|max:500',
            'mini_desc' => 'required|string|max:255',
            'desc' => 'required|string'
        ]);  
        
        $result = $this->admin_manager->editSet($slug);
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при изменении набора товаров']);
        
        return \Redirect::to('shopmanager/set/'.$result->slug)->with('messages',['Набор успешно изменен']);        
    }
    
    public function postDeleteset($slug, Request $request)
    {
        $result = $this->admin_manager->deleteSet($slug);
        if(!$result) return Redirect::back()->withErrors(['Не удалось удалить набор']);
        
        return \Redirect::back()->with('messages',['Набор успешно удален']);             
    }
    
    public function getSet($slug)
    {
        $set = $this->admin_manager->setInfo($slug);
        if(!$set) abort(404);
        
        \View::share('selmenu','sets');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/sets','Наборы товаров']));
        array_push($GLOBALS['breadcrumb'],(['#',$set->name]));
        
        return view('shop.admin.set_info',['set'=>$set]);
    }
    
    public function getSetaddgood($slug)
    {
        $set = $this->admin_manager->setInfo($slug);
        if(!$set) abort(404);
        
        $goods = $this->admin_manager->goodNameslist();
        
        \View::share('selmenu','sets');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/sets','Наборы товаров']));
        array_push($GLOBALS['breadcrumb'],(['shopmanager/set/'.$set->slug,$set->name]));
        array_push($GLOBALS['breadcrumb'],(['#','Добавить товар']));
        
        return view('shop.admin.set_addgood',['set'=>$set,'goods'=>$goods]);        
    }
    
    public function postSetaddgood($slug, Request $request)
    {
        $set = $this->admin_manager->setInfo($slug);
        if(!$set) abort(404);
        
        $this->validate($request, [
            'good_id' => 'required|numeric|exists:shop_goods,id',
            'count' => 'required|numeric|min:1',
        ]); 
        
        $result = $this->admin_manager->setAddGood($set->id);
        
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при добавлении товара к набору']);
        
        return \Redirect::to('shopmanager/set/'.$slug)->with('messages',['Товар добавлен к набору']);
    }
    
    public function getSeteditgood($slug,$id)
    {
        $set = $this->admin_manager->setInfo($slug);
        if(!$set) abort(404);
        
        $good = $this->admin_manager->setGoodInfo($id);
        if(!$good) abort(404);
        
        $goods = $this->admin_manager->goodNameslist();
        
        \View::share('selmenu','sets');
        array_push($GLOBALS['breadcrumb'],(['shopmanager/sets','Наборы товаров']));
        array_push($GLOBALS['breadcrumb'],(['shopmanager/set/'.$set->slug,$set->name]));
        array_push($GLOBALS['breadcrumb'],(['#','Изменить товар']));
        
        return view('shop.admin.set_editgood',['set'=>$set,'goods'=>$goods,'good'=>$good]);           
    }
    
    public function postSeteditgood($slug, $id, Request $request)
    {
        $set = $this->admin_manager->setInfo($slug);
        if(!$set) abort(404);
        
        $good = $this->admin_manager->setGoodInfo($id);
        if(!$good) abort(404);
        
        $this->validate($request, [
            'good_id' => 'required|numeric|exists:shop_goods,id',
            'count' => 'required|numeric|min:1',
        ]); 
        
        $result = $this->admin_manager->setEditGood($id);
        
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при изменении товара в наборе']);
        
        return \Redirect::to('shopmanager/set/'.$slug)->with('messages',['Товар изменен']);
    }
    
    public function postSetdeletegood($id, Request $request)
    {
        $result = $this->admin_manager->setDeleteGood($id);
        if(!$result) return Redirect::back()->withErrors(['Не удалось удалить товар из набор']);
        
        return \Redirect::back()->with('messages',['Товар удален из набора']);         
    }
    
    public function getEditfeedback($id)
    {
        $feedback = $this->admin_manager->feedbackInfo($id);
        if(!$feedback) abort(404);
        
        array_push($GLOBALS['breadcrumb'],(['#','Редактировать отзыв']));
        
        return view('shop.admin.feedback_edit',['feedback'=>$feedback]);
    }
    
    public function postEditfeedback($id, Request $request)
    {
        $feedback = $this->admin_manager->feedbackInfo($id);
        if(!$feedback) abort(404);
        
        $this->validate($request, [
            'text' => 'required|string'
        ]); 
        
        $result = $this->admin_manager->feedbackEdit($id);
        
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при изменении отзыва']);
        
        return \Redirect::to('shopmanager/'.$feedback->type.'/'.$feedback->info->slug)->with('messages',['Отзыв успешно изменен']);
    }
    
    public function postDeletefeedback($id, Request $request)
    {
        $feedback = $this->admin_manager->feedbackInfo($id);
        if(!$feedback) abort(404);
        
        $result = $this->admin_manager->feedbackDelete($id);
        if(!$result) return \Redirect::back()->withErrors(['Ошибка при удалении отзыва']);
        
        return \Redirect::back()->with('messages',['Отзыв успешно удален']);
    }
    
    public function getFilters()
    {
        //
    }
}
