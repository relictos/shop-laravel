<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\CartManager as Cart;

class CartController extends Controller
{
    private $cart_manager;
    
    public function __construct(Cart $cart)
    {
        $this->cart_manager = $cart;
        $this->middleware('auth:Makeorder', ['only' => ['getOrder']]);
        
        \View::share('selmenu','cart');
    }
    
    public function getIndex()
    {
        $cart = $this->cart_manager->fullInfo();
        
        return view('shop.cart.index',['cart'=>$cart]);
    }
    
    public function getOrder()
    {   
        if($this->cart_manager->isEmpty())
        {
            return redirect('cart')->withErrors(['Корзина пуста']);
        }
        
        $cart = $this->cart_manager->miniInfo();
        return view('shop.cart.makeorder',['cart'=>$cart]);
    }
    
    public function postOrder(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required|max:255',
            'comment' => 'max:255',
        ]);
        
        $order = $this->cart_manager->createOrder($request);
        if($order)
        {
            \Session::flash('messages',[trans('shop.ordercreated')]);
            return redirect('profile/orders');
        }
        
        return Redirect()->back()->withErrors(['Простите. У вас более 3 необработанных заказов. Пожалуйста, дождитесь, пока мы ответим на них']);
    }

}
