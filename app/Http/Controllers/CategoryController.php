<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\CatsManager as Cats;

class CategoryController extends Controller
{
    private $cats_manager;
    private $cat;
    
    public function __construct(Cats $cats)
    {
        $this->cats_manager = $cats;
        \View::share('selmenu','goods');
        
        array_push($GLOBALS['breadcrumb'],(['/','Главная']));
        array_push($GLOBALS['breadcrumb'],(['categories','Каталог товаров']));
    }
    
    public function getIndex($cat_slug)
    {
        $this->cat = $this->cats_manager->catInfo($cat_slug);
        if(!$this->cat) return abort(404);
        
        array_push($GLOBALS['breadcrumb'],(['#',$this->cat->name]));
        
        if($this->cat->parent_cat)
            return $this->getGoods();
        else
            return $this->getSubcats();
    }
    
    /* Список товаров */
    public function getGoods()
    {
        $filter = $this->cats_manager->getFilter($this->cat);
        $goods = $this->cats_manager->listGoods($this->cat,$filter);
        
        return view('shop.cats.goods_list',['cat'=>$this->cat,'goods'=>$goods,'filter'=>$filter]);
    }
    
    /* Список подкатегорий */
    public function getSubcats()
    {        
        return view('shop.cats.subcats_list',['cat'=>$this->cat]);
    }
    
    
    /* Список категорий */
    public function getList()
    {
        $cats = $this->cats_manager->catsList();
        
        return view('shop.cats.cats_list',['cats'=>$cats]);
    }
}
