<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class ShopController extends Controller
{
    public function getIndex()
    {
        \View::share('selmenu','main');
        return view('shop.main_page');
    }

}
