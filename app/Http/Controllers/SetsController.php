<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\SetsManager as Sets;

class SetsController extends Controller
{
    private $set;
    private $sets_manager;
    private $cat;
    
    public function __construct(Sets $sets)
    {
        $this->middleware('auth',['only' => ['postAddFeedback']]);
        
        $this->sets_manager = $sets;
        \View::share('selmenu','sets');
        
        array_push($GLOBALS['breadcrumb'],(['/','Главная']));
        array_push($GLOBALS['breadcrumb'],(['sets','Наборы']));
    }
    
    public function getIndex($set_slug)
    {  
        $this->set = $this->sets_manager->setInfo($set_slug);
        if(!$this->set) abort(404);
        
        array_push($GLOBALS['breadcrumb'],(['sets/'.$this->set->category->slug,$this->set->category->name]));
        array_push($GLOBALS['breadcrumb'],(['#',$this->set->name]));
        
        return view('shop.sets.set_info',['set'=>$this->set]);
    }  
    
    public function getList($cat_slug)
    {
        $cat = $this->sets_manager->catInfo($cat_slug);
        if(!$cat) abort(404);
        
        $sets = $this->sets_manager->listSets($cat);
        $cats = $this->sets_manager->listCats();
        
        array_push($GLOBALS['breadcrumb'],(['#',$cat->name]));
        
        return view('shop.sets.cat_info',['cat'=>$cat,'sets'=>$sets,'cats'=>$cats]);
    }  
    
    public function getCats()
    {
        $cats = $this->sets_manager->listCats();
        
        return view('shop.sets.cats_list',['cats'=>$cats]);
    }
    
    public function postAddFeedback($set_slug,Request $request)
    {
        $this->validate($request, [
            'feedback' => 'required'
        ]);
        
        if(!$this->sets_manager->addFeedback($set_slug))
        {
            return Redirect()->back()->withErrors(['Нельзя оставить больше одного отзыва для товара']);
        }
        
        return Redirect()->back()->with('messages',['Отзыв успешно отправлен']);
    }
}
