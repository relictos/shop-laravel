<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\OrdersManager as Orders;

class ProfileController extends Controller
{
    private $orders_manager;
    private $user;
    
    public function __construct(Orders $orders)
    {
        $this->middleware('auth');
        $this->orders_manager = $orders;
        
        $this->user = Auth::user();
        
        array_push($GLOBALS['breadcrumb'],(['/','Главная']));
        array_push($GLOBALS['breadcrumb'],(['profile','Личный кабинет']));
    }
    
    public function getIndex()
    {
        return view('shop.user.main',['user'=>$this->user]);
    }
    
    public function getOrders()
    {
        array_push($GLOBALS['breadcrumb'],(['#','Заказы']));
        
        $orders = $this->orders_manager->listOrders($this->user->id);
        return view('shop.user.orders',['user'=>$this->user,'orders'=>$orders]);
    }
    
    public function getEdit()
    {
        array_push($GLOBALS['breadcrumb'],(['#','Редактировать личные данные']));
        
        $this->user->social->fio_arr = explode(' ',$this->user->social->fio);
        return View('shop.user.edit_info',['user'=>$this->user]);
    }
    
    public function postEdit(Request $request)
    {
        $this->validate($request, [
            'phone' => 'max:50',
            'fio_sname' => 'max:75',
            'fio_name' => 'max:75',
            'fio_fname' => 'max:75',
        ]);
        
        $this->user->social->fio = implode(' ',[request('fio_sname'),request('fio_name'),request('fio_fname')]);
        $this->user->social->phone = request('phone');
        $this->user->social->save();
        
        return Redirect('profile')->with('messages',['Данные успешно изменены']);
    }
}
