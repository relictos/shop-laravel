<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\GoodsManager as Goods;

class GoodsController extends Controller
{
    private $good;
    private $goods_manager;
    
    public function __construct(Goods $goods)
    {
        $this->middleware('auth',['only' => ['postAddFeedback']]);
        
        $this->goods_manager = $goods;
        \View::share('selmenu','goods');
        
        array_push($GLOBALS['breadcrumb'],(['/','Главная']));
        array_push($GLOBALS['breadcrumb'],(['categories','Каталог товаров']));
    }
    
    public function getIndex($good_slug)
    {
        $this->good = $this->goods_manager->goodInfo($good_slug);
        if(!$this->good) abort(404);
        
        if($this->good->category)
            array_push($GLOBALS['breadcrumb'],(['category/'.$this->good->category->slug,$this->good->category->name]));
        
        array_push($GLOBALS['breadcrumb'],(['#',$this->good->name]));
        return view('shop.goods.good_info',['good'=>$this->good]);
    }
    
    public function getSearch()
    {
        $result = $this->goods_manager->searchgoods();
        if(!$result) $result = [];
        
        array_push($GLOBALS['breadcrumb'],(['#','Поиск']));
        return view('shop.goods.search',['goods'=>$result]);
    }
    
    public function postAddFeedback($good_slug,Request $request)
    {
        $this->validate($request, [
            'feedback' => 'required'
        ]);
        
        if(!$this->goods_manager->addFeedback($good_slug))
        {
            return Redirect()->back()->withErrors(['Нельзя оставить больше одного отзыва для товара']);
        }
        
        return Redirect()->back()->with('messages',['Отзыв успешно отправлен']);
    }

}
