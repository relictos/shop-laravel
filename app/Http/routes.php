<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controller('shop','ShopController');
Route::controller('profile','ProfileController');
Route::controller('shopmanager','AdminController');

Route::get('category/{cat_slug}', 'CategoryController@getIndex');
Route::get('categories', 'CategoryController@getList');

Route::get('/', 'ShopController@getIndex');

Route::get('goods/{good_slug}',['as'=>'good','uses'=>'GoodsController@getIndex']);
Route::post('goods/addfeedback/{good_slug}','GoodsController@postAddFeedback');

Route::get('search',['as'=>'search','uses'=>'GoodsController@getSearch']);

Route::get('set/{set_slug}',['as'=>'set','uses'=>'SetsController@getIndex']);
Route::post('set/addfeedback/{set_slug}','SetsController@postAddFeedback');

Route::get('sets/{cat_slug}',['as'=>'setcat','uses'=>'SetsController@getList']);
Route::get('sets',['as'=>'sets','uses'=>'SetsController@getCats']);

Route::get('cart','CartController@getIndex');

Route::get('makeorder','CartController@getOrder');
Route::post('makeorder','CartController@postOrder');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
