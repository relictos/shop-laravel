<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AuthAdmin {

    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    public function handle($request, Closure $next)
    {
        if($this->auth->user()->role != 'admin')
        {
            $this->auth->logout(); // optionally logout the user here
            return response(404);
        }

        return $next($request);
    }

}