<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'ordercreated' => 'Ваш заказ поступил в обработку. В ближайшее время с вами свяжется наш оператор. Статус заказа можно увидеть на этой странице'
];
