@extends('index')

@section('title')
    <title>Новый пароль</title>
    <meta name="description" content="Форма восстановления пароля"/>
@stop

@section('content')
<form method="POST" action="/password/reset">
    {!! csrf_field() !!}
    <input type="hidden" name="token" value="{{ $token }}">

    <div class="form-group">
        Email
        <input class="form-control" type="email" name="email" value="{{ old('email') }}">
    </div>

    <div class="form-group">
        Пароль
        <input class="form-control" type="password" name="password">
    </div>

    <div class="form-group">
        Подтверждение пароля
        <input class="form-control" type="password" name="password_confirmation">
    </div>

    <button type="submit" class="btn btn-primary">
        Восстановить пароль
    </button>
</form>
@stop