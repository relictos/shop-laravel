@extends('index')

@section('title')
    <title>Вход</title>
    <meta name="description" content="Форма входа в личный кабинет"/>
@stop

@section('content')
<form method="POST" action="/auth/login">
    {!! csrf_field() !!}
    
    <div class="form-group">
        <label>Email</label>
        <input class="form-control" type="email" name="email" value="{{ old('email') }}">
    </div>

    <div class="form-group">
        <label>Пароль</label>
        <input class="form-control" type="password" name="password" id="password">
    </div>

    <div class="form-group">
        <input type="checkbox" name="remember"> Запомнить меня
    </div>

    <button class="btn btn-primary" type="submit">Вход</button>
</form>
@stop