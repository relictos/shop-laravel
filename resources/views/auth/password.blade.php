@extends('index')

@section('title')
    <title>Восстановление пароля</title>
    <meta name="description" content="форма восстановления пароля"/>
@stop

@section('content')
<div class="page-header">
    <h1>Восстановление пароля</h1>
</div>

<form method="POST" action="/password/email">
    {!! csrf_field() !!}
    
    <p class="help-block">Пожалуйста, введите ваш адрес электронной почты, используемый для входа на сайт. Мы отправим вам письмо с дальнейшими инструкциями</p>
    <div class="form-group">
        <label>Email</label>
        <input class="form-control" type="email" name="email" value="{{ old('email') }}">
    </div>

    <button class="btn btn-primary" type="submit">
        Получить ссылку на восстановление пароля
    </button>
</form>
@stop