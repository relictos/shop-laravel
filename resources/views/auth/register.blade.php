@extends('index')

@section('title')
    <title>Регистрация</title>
    <meta name="description" content="Форма регистрации в магазине"/>
@stop

@section('content')
<form method="POST" action="/auth/register" class="form">
    {!! csrf_field() !!}

    <div class="form-group">
        <label>Email</label>
        <input class="form-control" type="email" name="email" value="{{ old('email') }}">
    </div>

    <div class="form-group">
        <label>Пароль</label>
        <input class="form-control" type="password" name="password">
    </div>

    <div class="form-group">
        <label>Подтверждение пароля</label>
        <input class="form-control" type="password" name="password_confirmation">
    </div>

    <button class="btn btn-primary" type="submit">Зарегистрироваться</button>
</form>
@stop