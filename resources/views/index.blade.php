<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="micheal" />
    
    @section('title')
	   <title>shop test 1.1</title>
       <meta name="description" content=""/>
    @show
    
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    
    <link href="/css/main.css" rel="stylesheet">
    
    <script src="/js/js.cookie.js"></script>
    <script src="/js/cart.js"></script>    
    
    @yield('assets')
    

</head>

<body>
    @yield('body_assets')
    
    @include('shop.vidgets.navbar_top')
    
    <nav class="navbar navbar-default" id="main_menu">
      <div class="container container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Навигация</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          @section('brand')
          <a class="navbar-brand" href="{{url('/')}}">
            <img src="/tiens.png" />
          </a>
          @show
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav" id="mm_left">
          @section('mainmenu')
            <li id="mi_main"><a href="{!! url('/') !!}">Главная</a></li>
            <li class="divider-vertical"></li>
            <li id="mi_goods">
              <a href="{!! url('categories') !!}">Товары</a>
            </li>
            <li class="divider-vertical"></li>
            <li id="mi_sets"><a href="{!! route('sets') !!}">Наборы <span class="label label-success">new</span></a></li>
          @show
          </ul>
          
          <script>
                var selmenu = "{{$selmenu or ''}}";
                
                $('#main_menu li').removeClass('active');
                $('#mi_'+selmenu).addClass('active');
          </script>
          
          <ul class="nav navbar-nav navbar-right">
          @section('right_nav')
            <li class="cart-btn" id="mi_cart">
                <a href="{{url('cart')}}">
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                    Корзина
                    <span class="badge" id="cartcount"></span>
                </a>
            </li>
          @show
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <div class="container">
        @include('shop.vidgets.breadcrumb')
    
        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">{{ $error }}</div>
            @endforeach
        @endif
        
        @if (Session::has('messages'))
            @foreach (Session::get('messages') as $mess)
                <div class="alert alert-success">{{ $mess }}</div>
            @endforeach
        @endif
    
        @yield('content')
        
        <script>
            $(function () {
              $('[data-toggle="tooltip"]').tooltip()
            });
        </script>
    </div>
    
    @section('footer')
    <div class="footer-inner"></div>
    <footer class="footer">
        <div class="footer-block container">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <h4>Магазин</h4>
                    <ul class="list-unstyled">
                        <li>О нас</li>
                        <li>Рекламодателям</li>
                        <li>Контакты</li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    <h4>Наши партнеры</h4>
                    <ul class="list-unstyled">
                        <li>Партнер первый</li>
                        <li>Еще партнер</li>
                        <li>Кто его знает, сколько их</li>
                    </ul>
                </div>
                <div class="col-md-3 col-xs-6">
                    <h4>Tienshi</h4>
                    <ul class="list-unstyled">
                        <li>О компании</li>
                        <li>Продукция</li>
                        <li>Клиенты</li>
                    </ul>
                </div>
                
                <link href="/css/buttons.css" rel="stylesheet">
                <div class="col-md-3 col-xs-6">
                    <h4>Мы в соц. сетях</h4>
                    <a class="btn btn-social-icon btn-vk"><span class="fa fa-vk"></span></a>
                    <a class="btn btn-social-icon btn-odnoklassniki"><span class="fa fa-odnoklassniki"></span></a>
                    <a class="btn btn-social-icon btn-instagram"><span class="fa fa-instagram"></span></a>
                    <a class="btn btn-social-icon btn-facebook"><span class="fa fa-facebook"></span></a>
                    <a class="btn btn-social-icon btn-twitter"><span class="fa fa-twitter"></span></a>
                    <a class="btn btn-social-icon btn-google"><span class="fa fa-google-plus"></span></a>
                </div>
            </div>
            <hr />
            <p>Абзац какогонибудь текста о всяких юридических делах и отказах об ответственности за некачественные тяжелые наркотики</p>
        </div>
    </footer>
    @show
</body>
</html>