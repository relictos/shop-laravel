<form method="GET" action="{{url('search')}}">
    <div class="input-group">
      <input type="text" name="request" class="form-control input-lg" placeholder="Поиск товара" value="{{(Request::has('request')) ? request('request') : ''}}">
      <span class="input-group-btn">
        <button type="submit" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-search"></span></button>
      </span>
    </div>
    <span class="help-block">Введите слово или фразу, которая должна содержаться в названии или описании товара</span>
</form>