@inject('goodsmanager', 'App\Services\GoodsManager')

<h3>Выгодные предложения</h3>
<div class="nodes-block">
    @foreach($goodsmanager->vidgetGoods(6) as $good)
    <div class="node-item">
        <div class="thumbnail">
              <div class="node-img-block">
                <a href="{!! route('good',$good->slug) !!}">
                    <img src="/img/{{$good->image}}">
                </a>
              </div>
              <div class="caption">
                <h3 class="good-name-text" data-toggle="tooltip" data-placement="bottom" title="{{$good->name}}"><a href="{!! route('good',$good->slug) !!}">{{$good->name}}</a></h3>
                <a href="{!! url('category/'.$good->category->slug) !!}">
                    <span class="help-block">{{$good->category->name}}</span>
                </a>
                <div class="price-info-text">{{$good->price}} руб.</div>
                
                <p>
                    <button class="btn btn-primary btn-buy" data-type="good" data-slug="{{$good->slug}}">
                        <span class="glyphicon glyphicon-shopping-cart"></span> В корзину
                    </button>
                    <a href="{!! route('good',$good->slug) !!}" class="btn btn-link" role="button">Подробнее...</a>
                </p>
              </div>
        </div>
    </div>
    @endforeach
</div>