@section('breadcrumb')
    @if($GLOBALS['breadcrumb'])
        <ol class="breadcrumb">
        @foreach($GLOBALS['breadcrumb'] as $bc)                  
          <li><a href="{{url($bc[0])}}">{{$bc[1]}}</a></li>
        @endforeach
        </ol>
        <script>$('.breadcrumb li:last-child').addClass('active')</script>
    @endif
@show