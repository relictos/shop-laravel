<nav class="navbar navbar-default navbar-top" id="topmenu">
    <div class="container container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topnavbar" aria-expanded="false">
                <span class="sr-only">Навигация</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
        </div>
        <div class="collapse navbar-collapse" id="topnavbar">
            <p class="navbar-text">
                <span class="glyphicon glyphicon-earphone"></span> <strong>+7 (343) 219-42-83</strong>
            </p>
            <ul class="nav navbar-nav navbar-right" id="mm_left">
                @if(Auth::check())
                    <li>
                      <a href="{!! url('profile') !!}">Личный кабинет</a>
                    </li>
                    <li>
                        <a href="{!! url('profile/orders') !!}">Заказы</a>
                    </li>
                @else
                    <li>
                      <a href="{!! url('auth/login') !!}">Вход</a>
                    </li>
                    <li>
                        <a href="{!! url('auth/register') !!}">Регистрация</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>