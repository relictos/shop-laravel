@inject('catsmanager', 'App\Services\CatsManager')

<div class="list-group">
  @forelse($catsmanager->catsList() as $cat)
      <div class="list-group-item">
        <div class="cat-text">
            <h4 class="list-group-item-heading">
                <a href="{!! url('category/'.$cat->slug) !!}">{{$cat->name}}</a>
                <a class="pull-right" data-toggle="collapse" href="#{{$cat->slug}}">
                    <span class="caret"></span>
                </a>
            </h4>
        </div>
      </div>
      

      @if($cat->subcats)
          <div id="{{$cat->slug}}" class="panel-collapse subcat-panel collapse in">
            <ul class="list-group">
                @foreach($cat->subcats as $sub)
                    <a href="{!! url('category/'.$sub->slug) !!}" class="list-group-item">{{$sub->name}}</a>
                @endforeach
            </ul>
          </div>  
      @endif
  @empty
    <p>Нет категорий</p>
  @endforelse
</div>