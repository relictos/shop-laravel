@inject('setsmanager','App\Services\SetsManager')

<div class="compl-slider">
    <h3 class="slider-title">
        Наборы товаров
        <div id="slider-controls"></div>
    </h3>                  
    <div id="sync1" class="owl-carousel">
    
    @foreach($setsmanager->vidgetSets() as $set)
	<div class="item">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4 col-xs-2">
                        <a href="{!! route('set',$set->slug) !!}">
                            <img alt="Slider" class="img img-rounded" src="/img/{{$set->image}}" width="100%"/>
                        </a>
                    </div>
                    <div class="col-sm-8">
                        <h3>
                            <b class="pull-right">{{$set->price}} руб.</b>
                            <a href="{!! route('set',$set->slug) !!}">{{$set->name}}</a>
                        </h3> 
                        <a href="{!! route('setcat',$set->category->slug) !!}">
                            <span class="help-block">{{$set->category->name}}</span>
                        </a>
                        <p>{{$set->mini_desc}}</p>
                        <button class="btn btn-primary btn-buy" data-type="set" data-slug="{{$set->slug}}">
                            <span class="glyphicon glyphicon-shopping-cart"></span> В корзину
                        </button>
                        <a href="{!! route('set',$set->slug) !!}" class="btn btn-link">Подробнее...</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
  </div>

</div>