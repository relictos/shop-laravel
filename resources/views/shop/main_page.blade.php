@extends('index')

@section('title')
    <title>Главная</title>
    <meta name="description" content="Добро пожаловать в магазин"/>
@stop

@section('assets')
    <link href="/css/nodes_list.css" rel="stylesheet">
    <link href="/css/owl.carousel.css" rel="stylesheet">
    <link href="/css/owl.theme.css" rel="stylesheet">
    <script src="/js/owl.carousel.min.js"></script>
    
    <link href="/css/sliders.css" rel="stylesheet">
    <script src="/js/slider_main.js"></script>
@stop

@section('content')
<div class="row">

    <div class="col-md-9" id="left_content">
        @include('shop.vidgets.search')
        
        @include('shop.vidgets.setslider')
        <hr />
        @include('shop.vidgets.goodlist')
        
    </div>   
    <div class="col-md-3 hidden-sm hidden-xs" id="right_vidget">
        @include('shop.vidgets.catslist')
    </div> 
</div>
@stop