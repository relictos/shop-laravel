﻿@extends('index')

@section('title')
    <title>{{$cat->name}}</title>
    <meta name="description" content="{{$cat->desc}}"/>
@stop

@section('assets')
    <link href="/css/nodes_list.css" rel="stylesheet">
@stop

@section('content')
        <div class="page-header">
            <h1>{{$cat->name}}</h1>
            <p class="help-block">{{$cat->desc}}</p>
        </div>
        <div class="nodes-block list-nodes-block">
            @forelse($cat->subcats as $scat)
                <a class="node-item" href="{!! url('category/'.$scat->slug) !!}">
                    <button class="btn btn-default btn-block btn-lg">{{$scat->name}} <span class="badge">{{$scat->goodsCount()}}</span></button>
                </a>
            @empty
                <p class="text-center">Подразделов не найдено</p>
            @endforelse
        </div>
@stop