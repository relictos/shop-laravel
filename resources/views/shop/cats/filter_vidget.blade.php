<button class="btn btn-default btn-lg btn-block btn-filter-collapse" data-toggle="collapse" data-target="#filterVidget" aria-expanded="false">
    <span class="glyphicon glyphicon-filter"></span>
    Фильтр поиска
</button>
<div class="collapse" id="filterVidget">
    <div class="panel panel-default">
        <div class="panel-body">
        @if($filter)
            <form class="form" method="GET" action="{{url('category/'.$cat->slug)}}">
                <input type="hidden" name="filter" value="on"/>
                <div class="form-group">
                    <label>Цена:</label>
                    <div class="value-interval">
                        <input type="text" name="price_min" class="form-control valmin" value="{{(request::has('price_min')) ? request('price_min') : '0'}}"/>
                        <span class="glyphicon glyphicon-minus"></span>
                        <input type="text" name="price_max" class="form-control valmax" value="{{(request::has('price_max')) ? request('price_max') : $filter->priceMax}}"/> руб.
                        <div class="values-slider" data-name="price" data-minval="0" data-maxval="{{$filter->priceMax}}">
                            
                        </div>
                    </div>
                </div>
                <hr />
                @if($filter->params)
                @foreach($filter->params as $param)
                <div class="form-group">
                    <label>{{$param->info->name}}</label>
                    @if($param->type == 'diap')
                        <div class="value-interval">
                            <input type="text" name="{{$param->info->slug}}_min" class="form-control valmin" value="{{(request::has($param->info->slug.'_min')) ? request($param->info->slug.'_min') : $param->values[0]}}"  />
                            <span class="glyphicon glyphicon-minus"></span>
                            <input type="text" name="{{$param->info->slug}}_max" class="form-control valmax" value="{{(request::has($param->info->slug.'_max')) ? request($param->info->slug.'_max') : $param->values[1]}}" />
                            {{$param->info->suffix}}
                            <div class="values-slider" data-name="{{$param->info->slug}}" data-minval="{{$param->values[0]}}" data-maxval="{{$param->values[1]}}">
                                
                            </div>
                        </div>
                       
                    @elseif($param->type == 'select')
                        <select name="{{$param->info->slug}}" class="form-control">
                            <option value="all">...</option>
                            @foreach($param->values as $val)
                                <option value="{{$val}}" {{(request($param->info->slug) == $val) ? 'selected' : ''}}>{{$val}}</option>
                            @endforeach
                        </select>    
                    @endif
                </div>
                @endforeach
                @endif

                <!--div class="form-group">
                    <label>Тип добавок:</label>
                    <div class="checklist">
                		<ul class="list-group checked-list-box">
                          <li class="list-group-item">Капсулы</li>
                          <li class="list-group-item" data-checked="true">Таблетки</li>
                          <li class="list-group-item">Порошок</li>
                          <li class="list-group-item">Серная кислота</li>
                          <li class="list-group-item">Цианид</li>
                          <li class="list-group-item">Коньяк</li>
                          <li class="list-group-item">Водка</li>
                        </ul>
                    </div>
                </div-->
                <hr />
                <button type="submit" class="btn btn-primary btn-lg btn-block">Применить</button>
            </form>
        @else
            <p class="text-center">Фильтр поиска не найден</p>
        @endif
        </div>
    </div>
</div>

<script>
    $('.values-slider').each(function(){
        var name = $(this).data('name');
        var aminval = $(this).data('minval'); //abs vals
        var amaxval = $(this).data('maxval');
        
        var minval = $('input[name="'+name+'_min"]').val();
        var maxval = $('input[name="'+name+'_max"]').val();
        
        var slider = noUiSlider.create(this, {
        	start: [ minval, maxval ],
        	connect: true,
        	range: {
        		'min': parseInt(aminval),
        		'max': parseInt(amaxval)
        	}
        });
        
        this.noUiSlider.on('update', function ( values, handle ) {
            if ( !handle ) {
        		$('input[name="'+name+'_min"]').val(Math.round(values[handle]));
        	} else {
        		$('input[name="'+name+'_max"]').val(Math.round(values[handle]));
        	}
        });
    });
</script>