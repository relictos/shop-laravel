@extends('index')

@section('title')
    <title>Каталог товаров</title>
    <meta name="description" content="<?php
        if($cats)
        {
            foreach($cats as $cat)
            { 
        ?>{{$cat->name}}, <?php        
            if($cat->subcats)
                {
                    foreach($cat->subcats as $scat)
                    {
        ?>{{$scat->name}}, <?php
                    }
                }
            }
        }
     ?>"/>
@stop

@section('assets')
    <link href="/css/nodes_list.css" rel="stylesheet">
@stop

@section('content')
<div class="page-header">
    <h1>Каталог товаров</h1>
</div>
<div class="nodes-block list-nodes-block">
    @forelse($cats as $cat)
        <div class="node-item">
            <div class="list-group">
                  <div class="list-group-item">
                    <div class="cat-text">
                        <h4 class="list-group-item-heading">
                            <a href="{!! url('category/'.$cat->slug) !!}">{{$cat->name}}</a>
                            <a class="pull-right btn btn-default btn-xs" data-toggle="collapse" href="#{{$cat->slug}}">
                                <span class="caret"></span>
                            </a>
                        </h4>
                        <p class="list-group-item-text">{{$cat->desc}}</p>
                    </div>
                  </div>
                  
            
                  @if($cat->subcats)
                      <div id="{{$cat->slug}}" class="panel-collapse subcat-panel collapse in">
                        <ul class="list-group">
                            @foreach($cat->subcats as $sub)
                                <a href="{!! url('category/'.$sub->slug) !!}" class="list-group-item">
                                    {{$sub->name}}
                                    <span class="badge">{{$sub->goodsCount()}}</span>
                                </a>
                            @endforeach
                        </ul>
                      </div>  
                  @endif
            </div>
        </div>
    @empty
        <p class="text-center">Категорий не найдено</p>
    @endforelse
</div>
@stop