@extends('index')

@section('title')
    <title>Результаты поиска</title>
    <meta name="description" content="Поиск товаров в магазине"/>
@stop

@section('assets')
    <link href="/css/nouislider.min.css" rel="stylesheet">
    <script src="/js/checklist.js"></script>
    
    <link href="/css/goods_list.css" rel="stylesheet">
@stop
@section('body_assets')
    <script src="/js/nouislider.min.js"></script>
@stop

@section('content')

@include('shop.vidgets.search')
<div class="page-header">
    <h1>Результаты поиска</h1>
</div>  


<div class="list-group goods-listgroup">
@forelse($goods as $good)
    <div class="list-group-item">
        <div class="list-group-item-text">
          <div class="row">
            <div class="col-sm-3 col-xs-3">
                <a href="{!! route('good',$good->slug) !!}">
                    <img src="/img/{{$good->image}}" class="img img-rounded" width="100%">
                </a>
            </div>
            <div class="col-sm-9">
              <div class="caption">
                <h3 class="goodname">
                    <b class="pull-right">{{$good->price}} руб.</b>
                    <a href="{!! route('good',$good->slug) !!}">{{$good->name}}</a> 
                </h3>
                <span class="help-block">
                    <a href="{{url('category/'.$good->category->slug)}}">{{$good->category->name}}</a>
                </span>
                
                <p>{{$good->mini_desc}}</p>
                <p>
                    <button class="btn btn-primary btn-buy" data-type="good" data-slug="{{$good->slug}}">
                        <span class="glyphicon glyphicon-shopping-cart"></span> В корзину
                    </button>
                    <a href="{!! route('good',$good->slug) !!}" class="btn btn-link" role="button">Подробнее...</a>
                </p>
              </div>
            </div>
          </div>
        </div>
    </div> 
@empty
    <p class="text-center">По вашему запросу ничего не найдено</p>
@endforelse
</div>


@if($goods)
<nav class="text-center">
  {!! $goods->appends(Request::all())->render() !!} 
</nav>
@endif
@stop

