@extends('index')

@section('title')
    <title>{{$good->name}}</title>
    <meta name="description" content="{{$good->mini_desc}}"/>
@stop

@section('assets')
    <link href="/css/good_desc.css" rel="stylesheet">
@stop

@section('content')
<div class="page-header">
    <h1>{{$good->name}}</h1>
</div>  
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-3 good-image">
                <img alt="Slider" class="img img-rounded" src="/img/{{$good->image}}"/>
            </div>
            <div class="col-sm-9">
                <button class="pull-right btn btn-success btn-lg buy-btn btn-buy" data-type="good" data-slug="{{$good->slug}}">
                    <span class="glyphicon glyphicon-shopping-cart"></span> В корзину
                </button>
                <h2>{{$good->price}}р.</h2> 
                <table class="props-table table table-striped" width=100%>
                    <thead>
                        <tr>
                            <th>Характеристики</th>
                        </tr>
                    </thead>
                    <tr>
                        <td>Раздел: </td>  
                        <td>
                            @if($good->category)
                                <a href="{!! url('category/'.$good->category->slug) !!}">{{$good->category->name}}</a>
                            @else
                                Без категории
                            @endif
                        </td>  
                    </tr>
                    @foreach($good->params as $param)
                        <tr>
                            <td>{{$param->param->name}}</td>
                            <td>{{$param->value}} {{$param->param->suffix}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

<ul class="nav nav-tabs nav-justified">
    <li class="active"><a href="#desc" data-toggle="tab">Описание</a></li>
    <li><a href="#feedbacks" data-toggle="tab">Отзывы <span class="label label-default">{{$good->feedbacks->count()}}</span></a></li>
</ul> 
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="desc">
        <div class="panel panel-default">
            <div class="panel-body">
            <h3>Описание {{$good->name}}</h3>
            {!! $good->desc !!}
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="feedbacks">
        <div class="panel panel-default">
            <div class="panel-body feedbacks-list">
                <a class="btn btn-default" href="#addfeedback">Добавить отзыв</a>
                <ul class="list-group">
                    @forelse($good->feedbacks as $fb)
                        <li class="list-group-item feedback-block">
                            <div class="list-group-item-text">
                                <h4 class="author">{{$fb->user->fio()}} <small>{{$fb->created_at->format('d.m.Y H:i')}}</small></h4>
                                {{$fb->text}}
                            </div>
                        </li>    
                    @empty
                        <p class="text-center">Отзывов пока нет</p>
                    @endforelse
                </ul>
                <hr />
                @if(Auth::check())
                <form class="form" method="POST" action="{{url('goods/addfeedback/'.$good->slug)}}" id="addfeedback">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label>Отзыв</label>
                        <textarea class="form-control" name="feedback" placeholder="Введите текст вашего отзыва"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить отзыв</button>
                </form>
                @else
                    <p>Чтобы добавлять отзывы к товарам, необходимо <a href="{{url('auth/login')}}">войти на сайт</a> или <a href="{{url('auth/register')}}">зарегистрироваться</a></p>
                @endif
            </div>
        </div>
    </div>
</div> 
@stop