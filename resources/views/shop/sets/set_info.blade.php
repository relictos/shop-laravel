@extends('index')

@section('title')
    <title>{{$set->name}}</title>
    <meta name="description" content="{{$set->mini_desc}}"/>
@stop

@section('assets')
    <link href="/css/good_desc.css" rel="stylesheet">
@stop

@section('content')
<div class="page-header">
    <h1>{{$set->name}}</h1>
    <p class="help-block">Раздел: <a href="{{ route('setcat',$set->category->slug) }}">{{$set->category->name}}</a></p>
</div>  
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-3">
                <img alt="Slider" class="img img-rounded" src="/img/{{$set->image}}" width="100%"/>
            </div>
            <div class="col-xs-9">
                <button class="pull-right btn btn-success btn-lg buy-btn btn-buy" data-type="set" data-slug="{{$set->slug}}">
                    <span class="glyphicon glyphicon-shopping-cart"></span> В корзину
                </button>
                <h2>{{$set->price}}р.</h2> 
                <p>{!!$set->desc!!}</p>
            </div>
        </div>
    </div>
</div>

<ul class="nav nav-tabs nav-justified">
    <li class="active"><a href="#desc"data-toggle="tab">Список товаров в наборе</a></li>
    <li><a href="#feedbacks" data-toggle="tab">Отзывы <span class="label label-default">{{$set->feedbacks->count()}}</span></a></li>
</ul> 
<div class="tab-content">
    <div class="tab-pane active" id="desc">
        <div class="panel panel-default goods-list">
            <div class="panel-body">
                <ul class="list-group">
                    @forelse($set->goods as $good)
                    <a href="{!! route('good',$good->good->slug) !!}" target="_blank" class="list-group-item">
                        <div class="list-group-item-text">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="/img/{{$good->good->image}}" width="100%"/>
                                </div>
                                <div class="col-xs-8">
                                    <h4 class="list-group-item-heading">{{$good->good->name}}</h3>
                                    <p class="help-block">{{$good->good->category->name}}</p>
                                    <p>{{$good->good->mini_desc}}</p>
                                </div>
                                <div class="col-xs-2">
                                    <p class="text-center">В наборе:</p>
                                    <p class="lead text-center"><span class="label label-primary">{{$good->count}} шт.</span></p>
                                </div>
                            </div>
                        </div>
                    </a>
                    @empty
                        <p class="text-center">Товаров не найдено</p>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="feedbacks">
        <div class="panel panel-default">
            <div class="panel-body feedbacks-list">
                <a class="btn btn-default" href="#addfeedback">Добавить отзыв</a>
                <ul class="list-group">
                    @forelse($set->feedbacks as $fb)
                        <li class="list-group-item feedback-block">
                            <div class="list-group-item-text">
                                <h4 class="author">{{$fb->user->fio()}} <small>{{$fb->created_at->format('d.m.Y H:i')}}</small></h4>
                                {{$fb->text}}
                            </div>
                        </li>    
                    @empty
                        <p class="text-center">Отзывов пока нет</p>
                    @endforelse
                </ul>
                <hr />
                @if(Auth::check())
                <form class="form" method="POST" action="{{url('set/addfeedback/'.$set->slug)}}" id="addfeedback">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label>Отзыв</label>
                        <textarea class="form-control" name="feedback" placeholder="Введите текст вашего отзыва"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить отзыв</button>
                </form>
                @else
                    <p>Чтобы добавлять отзывы к товарам, необходимо <a href="{{url('auth/login')}}">войти на сайт</a> или <a href="{{url('auth/register')}}">зарегистрироваться</a></p>
                @endif
            </div>
        </div>
    </div>
</div> 
@stop