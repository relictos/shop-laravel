@extends('index')

@section('title')
    <title>Наборы товаров</title>
    <meta name="description" content="<?php
        if($cats)
        {
            foreach($cats as $cat)
            { 
        ?>{{$cat->name}}, <?php
            }
        }
     ?>"/>
@stop

@section('assets')
    <link href="/css/nodes_list.css" rel="stylesheet">
@stop

@section('content')
<div class="page-header">
    <h1>Наборы</h1>
    <p class="help-block">Наборы товаров - это чтото там, что потом мы придумаем, как описать</p>
</div>
<div class="nodes-block list-nodes-block">
    <div class="list-group">
        @forelse($cats as $cat)
        <div class="node-item">
          <a href="{!! route('setcat',$cat->slug) !!}" class="list-group-item">
            <div class="cat-text">
                <h4 class="list-group-item-heading">
                    {{$cat->name}}
                </h4>
                <p class="list-group-item-text">{{$cat->desc}}</p>
            </div>
          </a>
        </div>
        @empty
            <p class="text-center">Категорий не найдено</p>
        @endforelse

    </div>
</div>
@stop