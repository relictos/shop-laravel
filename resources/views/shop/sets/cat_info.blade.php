@extends('index')

@section('title')
    <title>{{$cat->name}}</title>
    <meta name="description" content="{{$cat->desc}}"/>
@stop

@section('assets')
    <link href="/css/goods_list.css" rel="stylesheet">
@stop

@section('content')
<div class="page-header">
    <h1>{{$cat->name}}</h1>
    <p class="help-block">{{$cat->desc}}</p>
</div>  

<div class="list-group goods-listgroup">
@forelse($sets as $set)
<div class="list-group-item">
    <div class="list-group-item-text">
      <div class="row">
        <div class="col-sm-3 col-xs-2">
            <a href="{!! route('set',$set->slug) !!}">
                <img src="/img/{{$set->image}}" class="img img-rounded" width="100%">
            </a>
        </div>
        <div class="col-sm-9">
          <div class="caption">
            <h3 class="goodname">
                <b class="pull-right">{{$set->price}} руб.</b>
                <a href="{!! route('set',$set->slug) !!}">{{$set->name}}</a>
            </h3>
            <p>{{$set->mini_desc}}</p>
            <p class="record-set-items">
                @foreach($set->goods as $good)
                    <a href="{!! route('good',$good->good->slug) !!}" target="_blank" class="btn btn-sm btn-default">{{$good->good->name}} <b class="badge">x{{$good->count}}</b></a>
                @endforeach
            </p>
          </div>
          <p>
                <button class="btn btn-primary btn-buy" data-type="set" data-slug="{{$set->slug}}">
                    <span class="glyphicon glyphicon-shopping-cart"></span> В корзину
                </button>
                <a href="{!! route('set',$set->slug) !!}" class="btn btn-link" role="button">Подробнее...</a>
          </p>
        </div>
      </div>
    </div>
</div>

@empty
    <p class="text-center">Наборов не найдено</p>
@endforelse
</div>
      
<nav class="text-center">
    {!! $sets->render() !!} 
</nav>
@stop