@extends('index')

@section('title')
    <title>Оформить заказ</title>
    <meta name="description" content="форма отправки заказа"/>
@stop

@section('assets')
    <script src="/js/jquery.maskedinput.js"></script>
    <script src="/js/cart_ext.js"></script>
@stop

@section('right_nav')
<li class="cart-btn active">
    <a href="#">
        <span class="glyphicon glyphicon-shopping-cart"></span>
        Корзина
    </a>
</li>
@stop

@section('content')
<div class="page-header">
    <h1>Оформление заказа</h1>
</div>

<form method="POST" action="{{url('makeorder')}}">
    {!! csrf_field() !!}
    
    <div class="panel panel-default">
        <div class="panel-body">
            <h3 class="vidget-title">Контактные данные</h3>
            <hr />
            <div class="form-group">
                <label for="phone">Номер телефона</label>
                <input name="phone" type="text" class="form-control" id="phone" placeholder="Введите номер телефона">
                <p class="help-block">Номер, по которому с вами свяжется оператор для уточнения деталей заказа</p>
                <script> $("#phone").mask("+7(999) 999-9999");</script>
            </div>
            <div class="form-group">
                <label>Комментарий к заказу</label>
                <textarea class="form-control" name="comment" placeholder="Введите ваш комментарий к заказу..."></textarea>
            </div>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <h3 class="vidget-title">Список товаров</h3>  
            <hr />  
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Товар</th>
                        <th>Количество</th>
                        <th>Сумма</th>
                    </tr>
                </thead> 
                <tbody>
                    @foreach($cart as $good)
                    @if($good->info)
                    <tr>
                        <td scope="row">{{array_search($good,$cart)+1}}</td>
                        <td><a href="{{ route($good->type,$good->info->slug) }}" target="_blank">{{$good->info->name}}</a></td>
                        <td>{{$good->count}}</td>
                        <td class="{{$good->type}}-{{$good->info->slug}}-price" data-singleprice="{{$good->info->price}}">{{$good->info->price*$good->count}}р.</td>
                    </tr>
                    @endif
                    @endforeach
                    <tr class="success">
                        <td>#</td>
                        <td><strong>Итого:</strong></td>
                        <td><strong id="cartcount"></strong></td>
                        <td><strong><span id="cartprice"></span>р.</strong></td>
                    </tr>
                </tbody>   
            </table>
        </div>    
    </div>
    
    <button type="submit" class="btn btn-primary btn-lg btn-block">Отправить заказ</button>
</form>
@stop