@extends('index')

@section('title')
    <title>Корзина</title>
    <meta name="description" content="список товаров в корзине"/>
@stop

@section('assets')
    <link href="/css/cart.css" rel="stylesheet">
    <script src="/js/cart_ext.js"></script>
    <link href="/css/goods_list.css" rel="stylesheet">
@stop

@section('right_nav')
<li class="cart-btn active">
    <a href="#">
        <span class="glyphicon glyphicon-shopping-cart"></span>
        Корзина
    </a>
</li>
@stop

@section('content')
<div class="row">
    <div class="col-md-3"id="left_vidget">
        <div class="panel panel-default order-info-panel">
            <div class="panel-heading">Ваш заказ</div>
            <div class="panel-body">
                <p class="help-block">Сумма заказа:</p>
                <h2><span id="cartprice"></span>р.</h2> 
            </div>
            <ul class="list-group">
              <li class="list-group-item">
                Товаров в корзине: <b id="cartcount"></b>
              </li>
              <!--li class="list-group-item list-group-item-default">
                Скидка за комплект: <span class="label label-success">10%</span>
              </li-->
            </ul>
        </div>
        @if($cart)
        <a href="{{ url('makeorder') }}" class="btn btn-lg btn-primary btn-block">Оформить заказ</a>
        @endif
        
    </div>
    <div class="col-md-9">
        <div class="page-header">
          <h1>Моя корзина <small>список ваших покупок</small></h1>
        </div>    
        
        @if($cart)
        <div class="list-group goods-listgroup">
        @forelse($cart as $item)
        @if($item->info)
            @if($item->type == 'good')
                <div class="list-group-item">
                    <div class="list-group-item-text">  
                          <button type="button" class="close del-item" data-type="{{$item->type}}" data-slug="{{$item->info->slug}}"><span aria-hidden="true">&times;</span></button>
                          <div class="row">
                            <div class="col-sm-2 col-xs-2">
                              <img src="/img/{{$item->info->image}}" class="img img-rounded" width="100%">
                            </div>
                            <div class="col-sm-7 good-info-rec">
                              <div class="caption">
                                <h3 class="goodname"><a href="{!! route('good',$item->info->slug) !!}" target="_blank">{{$item->info->name}}</a></h3>
                                <span class="help-block">{{$item->info->category->name}}</span>
                                <p>{{$item->info->mini_desc}}</p>
                              </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <h4 class="order-item-price"><span class="{{$item->type}}-{{$item->info->slug}}-price" data-singleprice="{{$item->info->price}}">{{$item->info->price*$item->count}}</span>р.</h4>
                                <p class="help-block text-center">Количество</p>
                                <div class="input-group order-good-counter">
                                  <span class="input-group-btn">
                                    <button class="btn btn-danger btn-md btn-cartminus" data-type="{{$item->type}}" data-slug="{{$item->info->slug}}" type="button">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                  </span>
                                  <input type="text" class="form-control input-md text-center {{$item->type}}-{{$item->info->slug}}-count" value="{{$item->count}}" readonly>
                                  <span class="input-group-btn">
                                    <button class="btn btn-success btn-md btn-cartplus" data-type="{{$item->type}}" data-slug="{{$item->info->slug}}" type="button">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                  </span>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>
            @elseif($item->type == 'set')
                <div class="panel panel-default">
                    <div class="panel-body">
                          <button type="button" class="close del-item" data-type="{{$item->type}}" data-slug="{{$item->info->slug}}"><span aria-hidden="true">&times;</span></button>
                          <div class="row">
                            <div class="col-sm-3 col-xs-2">
                              <img src="/img/{{$item->info->image}}" class="img img-rounded" width="100%">
                            </div>
                            <div class="col-sm-6 good-info-rec">
                              <div class="caption">
                                <h3><a href="{!! route('set',$item->info->slug) !!}" target="_blank">{{$item->info->name}}</a></h3>
                                <p>{{$item->info->mini_desc}}</p>
                                <p class="record-set-items">
                                    @foreach($item->info->goods as $good)
                                        <a href="{!! route('good',$good->good->slug) !!}" target="_blank" class="btn btn-sm btn-default">{{$good->good->name}} <b class="badge">x{{$good->count}}</b></a>
                                    @endforeach
                                </p>
                              </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <h4 class="order-item-price"><span class="{{$item->type}}-{{$item->info->slug}}-price" data-singleprice="{{$item->info->price}}">{{$item->info->price*$item->count}}</span>р.</h4>
                                <p class="help-block text-center">Количество</p>
                                <div class="input-group order-good-counter">
                                  <span class="input-group-btn">
                                    <button class="btn btn-danger btn-md btn-cartminus" data-type="{{$item->type}}" data-slug="{{$item->info->slug}}" type="button">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                  </span>
                                  <input type="text" class="form-control input-md text-center {{$item->type}}-{{$item->info->slug}}-count" value="{{$item->count}}" readonly>
                                  <span class="input-group-btn">
                                    <button class="btn btn-success btn-md btn-cartplus" data-type="{{$item->type}}" data-slug="{{$item->info->slug}}" type="button">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                  </span>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>    
            @endif
        @endif
        @empty
            <p class="text-center">Корзина пуста</p>
        @endforelse
        </div>
        @else
            <p class="text-center">Корзина пуста</p>
        @endif
        
    </div>
</div>
@stop