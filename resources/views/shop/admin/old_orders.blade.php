@extends('shop.admin.index')


@section('title')
    <title>История заказов</title>
    <meta name="description" content="Список заказов"/>
@stop

@section('admin_content')
    <div class="page-header">
        <a href="{{url('shopmanager/orders')}}" class="pull-right btn btn-default btn-lg">Новые заказы</a>
        <h1>История заказов</h1>
    </div>
    
    @forelse($orders as $order)
        <div class="panel panel-default order-panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        
                        <a class="vidget-title"><span class="caret"></span> Заказ #{{$order->id}}</a>
                        ({{$order->created_at->format('d.m.Y')}})
                    </div>
                    <div class="col-md-3"><strong>{{$order->price}}р.</strong></div>
                    <div class="col-md-2">
                        <span class="{{$order->state->style}}">{{$order->state->name}}</span>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-info btn-xs btn-block" href="#order{{$order->id}}" data-toggle="collapse">Показать</a>
                    </div>
                </div>
            </div>
            <div class="list-group collapse" id="order{{$order->id}}">
                <div class="list-group-item">
                    <p>
                        <ul class="list-unstyled">
                            <li><strong>Покупатель:</strong> <a href="#">{{$order->user->social->fio or '-'}}</a></li>
                            <li><strong>Контактный телефон:</strong> {{$order->contact_phone}}</li>
                        </ul>
                    </p>
                    <p class="help-block"><strong>Комментарий покупателя: </strong> {{$order->user_comment}}</p>
                    <p class="help-block"><strong>Комментарий менеджера: </strong> {{$order->manager_comment}}</p>
                </div>
                @forelse($order->goods as $good)
                    <div class="list-group-item">
                        <div class="list-group-item-text">
                            <div class="row">
                                <div class="col-sm-1 col-xs-3">
                                    <img src="/img/{{$good->info->image}}" width="100%"/>
                                </div>
                                <div class="col-xs-9 col-sm-7">
                                    <h4 class="list-group-item-heading">
                                        <a href="{!! route($good->type,$good->info->slug) !!}" target="_blank">{{$good->info->name}}</a>
                                    </h3>
                                    <p class="help-block">{{$good->info->price}}р.</p>
                                </div>
                                <div class="col-sm-2">
                                    <span>Количество:</span>
                                    <p><b>{{$good->count}} шт.</b></p>
                                </div>
                                <div class="col-sm-2">
                                    <span>Стоимость:</span>
                                    <p><b>{{$good->info->price*$good->count}}р.</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <p class="text-center">Товаров не найдено</p>
                @endforelse
            </div>
        </div>
    @empty
        <p class="text-center">Новых заказов не найдено</p>
    @endforelse
    <div class="text-center">
        {!!$orders->render()!!}
    </div>

@stop