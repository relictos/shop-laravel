@extends('shop.admin.index')

@section('title')
    <title>Наборы товаров</title>
    <meta name="description" content="<?php
        if($cats)
        {
            foreach($cats as $cat)
            { 
        ?>{{$cat->name}}, <?php
            }
        }
     ?>"/>
@stop

@section('assets')
    <link href="/css/nodes_list.css" rel="stylesheet">
@stop

@section('admin_content')
<div class="page-header">
    <h1>Наборы</h1>
    <p class="help-block">Наборы товаров - это чтото там, что потом мы придумаем, как описать</p>
</div>

<a href="{{url('shopmanager/addsetcat')}}" class="pull-right btn btn-primary btn-lg">
    <span class="glyphicon glyphicon-plus"></span> Добавить категорию
</a>

<h3>Категории наборов</h3>
<div class="nodes-block list-nodes-block">
    <div class="list-group">
        @forelse($cats as $cat)
        <div class="node-item">
          <div class="list-group-item">
            <div class="cat-text">
                <div class="pull-right">
                    <a href="{{url('shopmanager/editsetcat/'.$cat->slug)}}" class="btn btn-default btn-xs">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <a href="#" data-href="{{url('shopmanager/delsetcat/'.$cat->slug)}}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#deleteCat">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </div>
                <h4 class="list-group-item-heading">
                    {{$cat->name}}
                </h4>
                <p class="list-group-item-text">{{$cat->desc}}</p>
            </div>
          </div>
        </div>
        @empty
            <p class="text-center">Категорий не найдено</p>
        @endforelse

    </div>
</div>

@include('shop.admin.modal.deleteCat')

<hr />
<a href="{{url('shopmanager/addset')}}" class="pull-right btn btn-primary btn-lg">
    <span class="glyphicon glyphicon-plus"></span> Добавить набор
</a>
<h3>Наборы товаров</h3>
<table class="table table-striped">
    <thead>
        <th>Название набора</th>
        <th>Категория</th>
        <th>Действия</th>
    </thead>
    @forelse($sets as $set)
        <tr>
            <td>
                <a href="{{url('shopmanager/set/'.$set->slug)}}">{{$set->name}}</a>
            </td>
            <td>
                {{$set->category->name}}
            </td>
            <td>
                <a href="{{url('shopmanager/editset/'.$set->slug)}}" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-edit"></span> Редактировать</a>
                <a href="#" data-href="{{url('shopmanager/deleteset/'.$set->slug)}}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#deleteSet">
                    <span class="glyphicon glyphicon-remove"></span> Удалить
                </a>
            </td>
        </tr>
    @empty
    @endforelse
</table>

@include('shop.admin.modal.deleteSet')

<nav class="text-center">
    {!! $sets->render() !!} 
</nav>
@stop