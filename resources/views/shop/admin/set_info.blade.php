@extends('shop.admin.index')

@section('title')
    <title>{{$set->name}}</title>
    <meta name="description" content="{{$set->mini_desc}}"/>
@stop

@section('assets')
    <link href="/css/good_desc.css" rel="stylesheet">
@stop

@section('admin_content')

@include('shop.admin.modal.deleteSet')
@include('shop.admin.modal.deleteSetGood')
@include('shop.admin.modal.deleteFb')

<div class="page-header">
    <a href="{{route('set',$set->slug)}}" target="_blank" class="pull-right btn btn-lg btn-default">Посмотреть в магазине</a>
    <h1>{{$set->name}}</h1>
</div>  
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-3">
                <img alt="Slider" class="img img-rounded" src="/img/{{$set->image}}" width="100%"/>
            </div>
            <div class="col-xs-9">
                <div class="pull-right">
                    <a href="{{url('shopmanager/editset/'.$set->slug)}}" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-edit"></span> Редактировать</a>
                    <a href="#" data-href="{{url('shopmanager/deleteset/'.$set->slug)}}" class="btn btn-default btn-lg" data-toggle="modal" data-target="#deleteSet">
                        <span class="glyphicon glyphicon-remove"></span> Удалить
                    </a>
                </div>
                <h2>{{$set->price}}р.</h2> 
                <p>{!!$set->desc!!}</p>
            </div>
        </div>
    </div>
</div>

<ul class="nav nav-tabs nav-justified">
    <li class="active"><a href="#desc"data-toggle="tab">Список товаров в наборе</a></li>
    <li><a href="#feedbacks" data-toggle="tab">Отзывы <span class="label label-default">{{$set->feedbacks->count()}}</span></a></li>
</ul> 
<div class="tab-content">
    <div class="tab-pane active" id="desc">
        <div class="panel panel-default">
            <div class="panel-body">
                <a class="pull-right btn btn-md btn-primary" href="{{url('shopmanager/setaddgood/'.$set->slug)}}">
                    <span class="glyphicon glyphicon-plus"></span> 
                    Добавить товар в набор
                </a>
                <table class="table">
                    <thead>
                        <th>Название товара</th>
                        <th>Категория</th>
                        <th>Количество</th>
                        <th>Действия</th>
                    </thead>
                    @forelse($set->goods as $good)
                    <tr>
                        <td>
                            <a href="{!! url('shopmanager/good/'.$good->good->slug) !!}" target="_blank">
                                {{$good->good->name}}
                            </a>
                        </td>
                        <td>{{$good->good->category->name}}</td>
                        <td>{{$good->count}} шт.</td>
                        <td>
                            <a href="{{url('shopmanager/seteditgood/'.$set->slug.'/'.$good->id)}}" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-edit"></span> Редактировать</a>
                            <a href="#" data-href="{{url('shopmanager/setdeletegood/'.$good->id)}}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#deleteSetGood">
                                <span class="glyphicon glyphicon-remove"></span> Удалить
                            </a>
                        </td>
                    </tr>
                    @empty
                        <p class="text-center">Товаров не найдено</p>
                    @endforelse
                </table>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="feedbacks">
        <div class="panel panel-default">
            <div class="panel-body feedbacks-list">
                <a class="btn btn-default" href="#addfeedback">Добавить отзыв</a>
                <ul class="list-group">
                    @forelse($set->feedbacks as $fb)
                        <li class="list-group-item feedback-block">
                            <div class="list-group-item-text">
                                <div class="pull-right">
                                    <a href="{{url('shopmanager/editfeedback/'.$fb->id)}}" class="btn btn-default btn-xs">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="#" data-href="{{url('shopmanager/deletefeedback/'.$fb->id)}}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#deleteFb">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </a>
                                </div>
                                
                                <h4 class="author">{{$fb->user->fio()}} <small>{{$fb->created_at->format('d.m.Y H:i')}}</small></h4>
                                {{$fb->text}}
                            </div>
                        </li>    
                    @empty
                        <p class="text-center">Отзывов пока нет</p>
                    @endforelse
                </ul>
                <hr />
                @if(Auth::check())
                <form class="form" method="POST" action="{{url('set/addfeedback/'.$set->slug)}}" id="addfeedback">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label>Отзыв</label>
                        <textarea class="form-control" name="feedback" placeholder="Введите текст вашего отзыва"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить отзыв</button>
                </form>
                @else
                    <p>Чтобы добавлять отзывы к товарам, необходимо <a href="{{url('auth/login')}}">войти на сайт</a> или <a href="{{url('auth/register')}}">зарегистрироваться</a></p>
                @endif
            </div>
        </div>
    </div>
</div> 
@stop