@extends('shop.admin.index')

@section('admin_content')
    <div class="page-header">
        <h2>Добавить товар к набору</h2>
    </div>
    <form method="POST" action="{{url('shopmanager/setaddgood/'.$set->slug)}}">
        {{csrf_field()}}
        <div class="form-group">
            <label>Товар</label>
            <select name="good_id" class="form-control">
                @foreach($goods as $good)
                    <option value="{{$good->id}}">{{$good->name}}</option>
                @endforeach
            </select>
            <script>
                $('[name=good_id]').val({{old('good_id')}});
            </script>
        </div>
        <div class="form-group">
            <label>Количество</label>
            <input class="form-control" name="count" value="{{old('count')}}" placeholder="Количество выбранных товаров в наборе"/>
        </div>
        
        <button type="submit" class="btn btn-primary btn-block btn-lg">Добавить товар к набору</button>
    </form>
@stop