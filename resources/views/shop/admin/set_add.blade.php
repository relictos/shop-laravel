@extends('shop.admin.index')

@section('assets')
    <script src="/js/jquery.synctranslit.min.js"></script>
    <script src="/js/tinymce/tinymce.min.js"></script>
@stop

@section('admin_content')
    <div class="page-header">
        <h1>Добавить набор товаров</h1>
    </div>
    <form method="POST" action="{{url('shopmanager/addset')}}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="form-group">
            <label>Название набора</label>
            <input type="text" name="name" class="form-control" placeholder="Введите название категории товаров" value="{{old('name')}}"/>
        </div>
        <div class="form-group">
            <label>Ссылка на набор</label>
            <input type="text" name="slug" id="slug" class="form-control" placeholder="Будет сгенерирована автоматически" value="{{old('slug')}}"/>
        </div>
        <script>
            $(document).ready(function(){
                $('input[name=name]').syncTranslit({destination: 'slug'});
            });
        </script>
        
        <div class="form-group">
            <label>Цена набора (руб.)</label>
            <input type="text" name="price" class="form-control" placeholder="Введите цену набора" value="{{old('price')}}"/>
        </div>
        
        <div class="form-group">
            <label>Категория набора</label>
            <select name="cat_id" class="form-control">
                @if($cats)
                @foreach($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                @endforeach
                @endif
            </select>
        </div>
        
        <div class="form-group">
            <label>Мини-описание набора (не более 255 символов)</label>
            <textarea class="form-control" name="mini_desc" placeholder="Введите мини-описание набора">{{old('mini_desc')}}</textarea>
            <p class="help-block">Отображается в списке наборов на сайте</p>
        </div>
        <div class="form-group">
            <label>Описание набора</label>
            <textarea class="form-control" name="desc" placeholder="Введите описание набора">{{old('desc')}}</textarea>
            
            <script>
                tinymce.init({
                  selector: 'textarea[name=desc]',
                  language: 'ru',
                  plugins : 'advlist autolink link image lists charmap print preview youtube',
                  toolbar: 'undo, redo | bold, italic, underline, strikethrough | alignleft, aligncenter, alignright, alignjustify | styleselect, | bullist, numlist, outdent, indent, subscript, superscript, removeformat  | youtube'
                });  
            </script>
        </div>
        
        <div class="form-group">
            <label>Изображение набора</label>
            <input type="file" name="image" class="form-control"/>
            <p class="help-block">Максимальный размер загружаемого файла: 500КБ. Форматы: JPG, PNG, GIF</p>
        </div>
        
        <button type="submit" class="btn btn-primary btn-lg btn-block">Создать набор товаров</button>
    </form>
@stop