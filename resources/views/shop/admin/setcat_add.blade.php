@extends('shop.admin.index')

@section('assets')
    <script src="/js/jquery.synctranslit.min.js"></script>
@stop

@section('admin_content')
    <div class="page-header">
        <h1>Добавить категорию (раздел) наборов</h1>
    </div>
    <form method="POST" action="{{url('shopmanager/addsetcat')}}">
        {!! csrf_field() !!}
        <div class="form-group">
            <label>Название категории</label>
            <input type="text" name="name" class="form-control" placeholder="Введите название категории товаров" value="{{old('name')}}"/>
        </div>
        <div class="form-group">
            <label>Ссылка на категорию</label>
            <input type="text" name="slug" id="slug" class="form-control" placeholder="Будет сгенерирована автоматически" value="{{old('slug')}}"/>
        </div>
        <script>
            $(document).ready(function(){
                $('input[name=name]').syncTranslit({destination: 'slug'});
            });
        </script>
        
        <div class="form-group">
            <label>Описание категории (не более 255 символов)</label>
            <textarea class="form-control" name="desc">{{old('desc')}}</textarea>
        </div>
        
        <button type="submit" class="btn btn-primary btn-lg btn-block">Создать категорию</button>
    </form>
@stop