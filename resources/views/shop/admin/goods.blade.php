@extends('shop.admin.index')

@section('title')
    <title>Товары</title>
    <meta name="description" content="Управление сайтом"/>
@stop

@section('admin_content')
    <div class="page-header">
        <a href="{{url('shopmanager/addgood')}}" class="pull-right btn btn-primary btn-lg"><span class="glyphicon glyphicon-plus"></span> Добавить товар</a>
        <h1>Товары</h1>
    </div>

    <form method="GET" action="{{url('shopmanager/goods')}}">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label>Категория</label>
                    <select name="cat_id" class="form-control">
                        <option value="-1">Все</option>
                        <option value="0">Без категории</option>
                        @foreach($cats as $cat)
                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                    </select>
                    <script>
                        $('[name=cat_id]').val({{request('cat_id')}});
                    </script>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Поиск</label>
                    <input type="text" class="form-control" name="search" placeholder="Введите поисковую фразу" value="{{request('search')}}"/>
                </div>
            </div>
            <div class="col-sm-3">
                <label>Применить</label>
                <button type="submit" class="btn btn-default btn-block"><span class="glyphicon glyphicon-search"></span> Поиск</button>
            </div>
        </div>    
    </form>
    
    @if($goods)
        <table class="table">
            <thead>
                <tr>
                    <th>Товар</th>
                    <th>Категория</th>
                    <th>Действия</th>
                    <th>Свойства</th>
                </tr>
            </thead>
            @foreach($goods as $good)
            <tr>
                <td width=50%>
                    <a href="{{url('shopmanager/good/'.$good->slug)}}">{{$good->name}}</a>
                </td>
                <td>
                    {{$good->category->name or 'Без категории'}}
                </td>
                <td>
                    <a href="{{url('shopmanager/editgood/'.$good->slug)}}" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-edit"></span> Редактировать</a>
                    <a href="#" data-href="{{url('shopmanager/deletegood/'.$good->slug)}}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#deleteFile">
                        <span class="glyphicon glyphicon-remove"></span> Удалить
                    </a>
                </td>
                <td>
                    <a class="btn btn-info btn-xs btn-block" href="#good{{$good->id}}" data-toggle="collapse">Свойства</a>
                </td>
            </tr>
            <tr id="good{{$good->id}}" class="active collapse">
                <td colspan=4>
                    <a href="{{url('shopmanager/addgoodparam/'.$good->slug)}}" class="btn btn-primary btn-xs pull-right"><span class="glyphicon glyphicon-plus"></span> Добавить свойство</a>
                    <label>Свойства товара</label>
                    <div class="panel panel-default">
                        <table class="table">
                            @foreach($good->params as $param)
                                <tr>
                                    <td>{{$param->param->name}}</td>
                                    <td>{{$param->value}} {{$param->param->suffix}}</td>
                                    <td>
                                        <a href="{{url('shopmanager/editgoodparam/'.$good->slug.'/'.$param->id)}}" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-edit"></span> Редактировать</a>
                                        <a href="#" data-href="{{url('shopmanager/deletegoodparam/'.$good->slug.'/'.$param->id)}}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#deleteParam">
                                            <span class="glyphicon glyphicon-remove"></span> Удалить
                                        </a>    
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </td>
            </tr>
            @endforeach
        </table>
        <div class="text-center">
            {!! $goods->appends(Request::all())->render() !!} 
        </div>
    @else
        <div class="text-center">Товаров не найдено</div>
    @endif
    
    @include('shop.admin.modal.deleteFile')
    @include('shop.admin.modal.deleteParam')
@stop