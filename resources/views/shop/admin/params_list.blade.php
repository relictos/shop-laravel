@extends('shop.admin.index')

@section('admin_content')
    <div class="page-header">
        <a href="#" data-href="{{url('shopmanager/createparam')}}" class="pull-right btn btn-primary btn-lg" data-toggle="modal" data-target="#createParam">
                <span class="glyphicon glyphicon-plus"></span> Создать характеристику
        </a>
        <h1>Характеристики товаров</h1>
        <p class="help-block">Список характеристик, доступных для товаров</p>
    </div>
    <table class="table table-striped">
        <thead>
            <th>Название</th>
            <th>Ед. измерения</th>
            <th>Используется (кол-во товаров)</th>
            <th></th>
        </thead> 
        @foreach($params as $param)
            <tr>
                <td>{{$param->name}}</td>
                <td>{{$param->suffix}}</td>
                <td>{{$param->goodscount()}}</td>
                <td>
                    <a href="{{url('shopmanager/editparam/'.$param->id)}}" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-edit"></span> Редактировать</a>
                    <a href="#" data-href="{{url('shopmanager/deleteparam/'.$param->id)}}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#deleteParam">
                        <span class="glyphicon glyphicon-remove"></span> Удалить
                    </a>  
                </td>
            </tr>
        @endforeach   
    </table>    
    
    @include('shop.admin.modal.deleteParam')
    @include('shop.admin.modal.createParam')
@stop