@extends('shop.admin.index')

@section('title')
    <title>{{$good->name}}</title>
    <meta name="description" content="{{$good->mini_desc}}"/>
@stop

@section('assets')
    <link href="/css/good_desc.css" rel="stylesheet">
@stop

@section('admin_content')

@include('shop.admin.modal.deleteFile')
@include('shop.admin.modal.deleteParam')
@include('shop.admin.modal.deleteFb')

<div class="page-header">
    <a href="{{route('good',$good->slug)}}" target="_blank" class="pull-right btn btn-lg btn-default">Посмотреть в магазине</a>
    <h1>{{$good->name}}</h1>
</div>  
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-3 good-image">
                <img alt="Slider" class="img img-rounded" src="/img/{{$good->image}}" />
            </div>
            <div class="col-sm-9">
                <div class="pull-right">
                    <a href="{{url('shopmanager/editgood/'.$good->slug)}}" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-edit"></span> Редактировать</a>
                    <a href="#" data-href="{{url('shopmanager/deletegood/'.$good->slug)}}" class="btn btn-default btn-lg" data-toggle="modal" data-target="#deleteFile">
                        <span class="glyphicon glyphicon-remove"></span> Удалить
                    </a>
                </div>
                
                <h2>{{$good->price}}р.</h2> 
                <table class="props-table table table-striped" width=100%>
                    <thead>
                        <tr>
                            <th colspan="3">
                                <a href="{{url('shopmanager/addgoodparam/'.$good->slug)}}" class="btn btn-primary btn-xs pull-right"><span class="glyphicon glyphicon-plus"></span> Добавить свойство</a>
                                Характеристики
                            </th>
                        </tr>
                    </thead>
                    <tr>
                        <td>Раздел: </td>  
                        <td>
                        @if($good->category)
                        {{$good->category->name}}
                        @else
                            Без категории
                        @endif
                        </td>  
                        <td></td>
                    </tr>
                    @foreach($good->params as $param)
                        <tr>
                            <td>{{$param->param->name}}</td>
                            <td>{{$param->value}} {{$param->param->suffix}}</td>
                            <td>
                                <a href="{{url('shopmanager/editgoodparam/'.$good->slug.'/'.$param->id)}}" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-edit"></span> Редактировать</a>
                                <a href="#" data-href="{{url('shopmanager/deletegoodparam/'.$good->slug.'/'.$param->id)}}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#deleteParam">
                                    <span class="glyphicon glyphicon-remove"></span> Удалить
                                </a>    
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

<ul class="nav nav-tabs nav-justified">
    <li class="active"><a href="#desc" data-toggle="tab">Описание</a></li>
    <li><a href="#feedbacks" data-toggle="tab">Отзывы <span class="label label-default">{{$good->feedbacks->count()}}</span></a></li>
</ul> 
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="desc">
        <div class="panel panel-default">
            <div class="panel-body">{!!$good->desc!!}</div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="feedbacks">
        <div class="panel panel-default">
            <div class="panel-body feedbacks-list">
                <a class="btn btn-default" href="#addfeedback">Добавить отзыв</a>
                <ul class="list-group">
                    @forelse($good->feedbacks as $fb)
                        <li class="list-group-item feedback-block">
                            <div class="list-group-item-text">
                                <div class="pull-right">
                                    <a href="{{url('shopmanager/editfeedback/'.$fb->id)}}" class="btn btn-default btn-xs">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a href="#" data-href="{{url('shopmanager/deletefeedback/'.$fb->id)}}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#deleteFb">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </a>
                                </div>
                                
                                <h4 class="author">{{$fb->user->fio()}} <small>{{$fb->created_at->format('d.m.Y H:i')}}</small></h4>
                                {{$fb->text}}
                            </div>
                        </li>    
                    @empty
                        <p class="text-center">Отзывов пока нет</p>
                    @endforelse
                </ul>
                <hr />
                @if(Auth::check())
                <form class="form" method="POST" action="{{url('goods/addfeedback/'.$good->slug)}}" id="addfeedback">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label>Отзыв</label>
                        <textarea class="form-control" name="feedback" placeholder="Введите текст вашего отзыва"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить отзыв</button>
                </form>
                @else
                    <p>Чтобы добавлять отзывы к товарам, необходимо <a href="{{url('auth/login')}}">войти на сайт</a> или <a href="{{url('auth/register')}}">зарегистрироваться</a></p>
                @endif
            </div>
        </div>
    </div>
</div> 
@stop