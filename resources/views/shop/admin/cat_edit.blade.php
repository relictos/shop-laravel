@extends('shop.admin.index')

@section('assets')
    <script src="/js/jquery.synctranslit.min.js"></script>
@stop

@section('admin_content')
    <div class="page-header">
        <h1>Изменить категорию (раздел) товаров</h1>
    </div>
    <form method="POST" action="{{url('shopmanager/editcat/'.$cat->slug)}}">
        {!! csrf_field() !!}
        <div class="form-group">
            <label>Название категории</label>
            <input type="text" name="name" class="form-control" placeholder="Введите название категории товаров" value="{{(old('name')) ? old('name') : $cat->name}}"/>
        </div>
        <div class="form-group">
            <label>Ссылка на категорию</label>
            <input type="text" name="slug" id="slug" class="form-control" placeholder="Будет сгенерирована автоматически" value="{{(old('slug')) ? old('slug') : $cat->slug}}"/>
        </div>
        <script>
            $(document).ready(function(){
                $('input[name=name]').syncTranslit({destination: 'slug'});
            });
        </script>
        
        <div class="form-group">
            <label>Родительская категория</label>
            <select name="parent_cat" class="form-control">
            @if($cat->parent_cat == 0)
                <option value="0">Без категории</option>
            @else
                @if($cats)
                    @foreach($cats as $scat)
                        <option value="{{$scat->id}}">{{$scat->name}}</option>
                    @endforeach
                @endif
            @endif
            </select>
            <script>
                $('[name=parent_cat]').val('{{$cat->parent_cat}}')
            </script>
            <p class="help-block">При выборе "без категории" ваша категория сама становится родительсткой. Это окончательное действие, которое невозможно изменить при редактировании.</p>
        </div>
        
        <div class="form-group">
            <label>Описание категории (не более 255 символов)</label>
            <textarea class="form-control" name="desc">{{(old('desc')) ? old('desc') : $cat->desc}}</textarea>
        </div>
        
        <button type="submit" class="btn btn-primary btn-lg btn-block">Изменить категорию</button>
    </form>
@stop