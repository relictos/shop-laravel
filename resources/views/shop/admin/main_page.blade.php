@extends('shop.admin.index')


@section('title')
    <title>Панель администратора</title>
    <meta name="description" content="Управление сайтом"/>
@stop

@section('admin_content')
    <div class="page-header">
        <h1>Панель администратора</h1>
    </div>
    <h3 class="vidget-title">
        <a data-toggle="collapse" href="#orders">
            Заказы <span class="caret"></span>
        </a>
        <div class="pull-right">
            <a class="btn btn-default" href="{{url('shopmanager/orders')}}">
                Новые заказы <div class="label label-danger">{{($order_stat->new_count > 0) ? $order_stat->new_count : ''}}</div>
            </a>
              
            <a class="btn btn-default" href="{{url('shopmanager/ordersold')}}">
                История заказов
            </a>
        </div>
    </h3>
    
    <div id="orders" class="collapse in">
        <div class="panel panel-default">    
            <table class="table">
                <thead>
                    <th></th>
                    <th>Сегодня</th>
                    <th>Вчера</th>
                    <th>Неделя</th>
                    <th>Месяц</th>
                    <th>Год</th>
                </thead>
                <tbody>
                    <tr>
                        <th>Количество</th>
                        <td>{{$order_stat->today['count']}}</td>
                        <td>{{$order_stat->yesterday['count']}}</td>
                        <td>{{$order_stat->week['count']}}</td>
                        <td>{{$order_stat->month['count']}}</td>
                        <td>{{$order_stat->year['count']}}</td>
                    </tr>
                    <tr>
                        <th>Сумма</th>
                        <td>{{$order_stat->today['sum']}} руб.</td>
                        <td>{{$order_stat->yesterday['sum']}} руб.</td>
                        <td>{{$order_stat->week['sum']}} руб.</td>
                        <td>{{$order_stat->month['sum']}} руб.</td>
                        <td>{{$order_stat->year['sum']}} руб.</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <h3 class="vidget-title">
        <a data-toggle="collapse" href="#goods">
            Товары <span class="caret"></span>
        </a>
    </h3>
    
    <div class="collapse in" id="goods">
        <div class="panel panel-default">    
            <table class="table">
                <thead>
                    <th>Всего товаров</th>
                    <th>Всего категорий</th>
                    <th>Всего наборов</th>
                    <th>Товаров в наборах</th>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$good_stat->goods_count}}</td>
                        <td>{{$good_stat->cats_count}}</td>
                        <td>{{$good_stat->sets_count}}</td>
                        <td>{{$good_stat->goods_insets}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop