@extends('shop.admin.index')

@section('admin_content')
    <div class="page-header">
        <h2>Редактировать отзыв</h2>
    </div>
    <form method="POST" action="{{url('shopmanager/editfeedback/'.$feedback->id)}}">
        {{csrf_field()}}
        <div class="form-group">
            <label>Текст отзыва</label>
            <textarea name="text" class="form-control">{{(old('text')) ? old('text') : $feedback->text}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary btn-block btn-lg">Изменить отзыв</button>
    </form>
@stop