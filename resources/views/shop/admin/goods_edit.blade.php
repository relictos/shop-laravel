@extends('shop.admin.index')

@section('assets')
    <script src="/js/jquery.synctranslit.min.js"></script>
    <script src="/js/tinymce/tinymce.min.js"></script>
@stop

@section('admin_content')
    <div class="page-header">
        <h1>{{$good->name}}</h1>
    </div>
    <form method="POST" action="{{url('shopmanager/editgood/'.$good->slug)}}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="form-group">
            <label>Название товара</label>
            <input type="text" class="form-control" name="name" placeholder="Введите название товара" value="{{(old('name')) ? old('name') : $good->name}}" />
        </div>
        <div class="form-group">
            <label>Ссылка на товар</label>
            <input type="text" class="form-control" name="slug" id="slug" placeholder="Здесь будет ссылка на товар" value="{{(old('slug')) ? old('slug') : $good->slug}}"/>
        </div>
        <script>
            $(document).ready(function(){
                $('input[name=name]').syncTranslit({destination: 'slug'});
            });
        </script>
        
        <div class="form-group">
            <label>Раздел</label>
            <select class="form-control" name="cat_id" value="{{old('cat_id')}}">
                @if($cats)
                @foreach($cats as $cat)
                    <option value="{{$cat->id}}" {{($good->cat_id == $cat->id) ? 'selected' : ''}}>{{$cat->name}}</option>
                @endforeach
                @endif
            </select>
        </div>
        
        <div class="form-group">
            <label>Цена (руб.)</label>
            <input type="text" class="form-control" name="price" placeholder="Цена товара (только цифры)" value="{{(old('price')) ? old('price') : $good->price}}"/>
        </div>
        
        <div class="form-group">
            <label>Описание товара</label>
            <textarea name="desc" class="form-control" placeholder="Полное описание товара. Выводится на странице товара">{{(old('desc')) ? old('desc') : $good->desc}}</textarea>
        
            <script>
                tinymce.init({
                  selector: 'textarea[name=desc]',
                  language: 'ru',
                  plugins : 'advlist autolink link image lists charmap print preview youtube',
                  toolbar: 'undo, redo | bold, italic, underline, strikethrough | alignleft, aligncenter, alignright, alignjustify | styleselect, | bullist, numlist, outdent, indent, subscript, superscript, removeformat  | youtube'
                });  
            </script>
        </div>
        
        <div class="form-group">
            <label>Мини-описание товара (255 символов)</label>
            <textarea name="mini_desc" class="form-control" placeholder="Мини-описание товара. Выводится в списке товаров.">{{(old('mini_desc')) ? old('mini_desc') : $good->mini_desc}}</textarea>
        </div>
        
        <div class="form-group">
            <label>Изображение товара</label>
            <div class="row">
                <div class="col-md-4">
                    <img src="/img/{{$good->image}}" width="100%"/>
                </div>
                <div class="col-md-8">
                    <input type="file" name="image" class="form-control"/>
                    <p class="help-block">Максимальный размер загружаемого файла: 500КБ. Форматы: JPG, PNG, GIF</p>    
                </div>
            </div>
        </div>
        
        <button class="btn btn-lg btn-primary btn-block">Изменить товар</button>
        
    </form>
@stop