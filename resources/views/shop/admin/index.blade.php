@extends('index')

@section('brand')

@stop

@section('title')
    <title>Панель управления</title>
    <meta name="description" content="Управление сайтом"/>
@stop

@section('mainmenu')
    <li class="divider-vertical"></li>
    <li id="mi_main"><a href="{!! url('shopmanager') !!}">Главная</a></li>
    <li class="divider-vertical"></li>
    <li id="mi_orders">
      <a href="{!! url('shopmanager/orders') !!}">Заказы</a>
    </li>
    <li class="divider-vertical"></li>
    <li id="mi_cats">
      <a href="{!! url('shopmanager/categories') !!}">Категории</a>
    </li>
    <li class="divider-vertical"></li>
    <li id="mi_goods">
      <a href="{!! url('shopmanager/goods') !!}">Товары</a>
    </li>
    <li class="divider-vertical"></li>
    <li id="mi_params">
      <a href="{!! url('shopmanager/params') !!}">Характеристики</a>
    </li>
    <li class="divider-vertical"></li>
    <li id="mi_sets">
        <a href="{!! url('shopmanager/sets') !!}">Наборы <span class="label label-success">new</span></a>
    </li>
@stop
@section('right_nav')
    <li>
        <a href="{{url('/')}}">
            <span class="glyphicon glyphicon-share"></span>
            На сайт
        </a>
    </li>
@stop

@section('content')
    @yield('admin_content')
@stop