@extends('shop.admin.index')

@section('title')
    <title>Разделы товаров</title>
    <meta name="description" content="<?php
        if($cats)
        {
            foreach($cats as $cat)
            { 
        ?>{{$cat->name}}, <?php        
            if($cat->subcats)
                {
                    foreach($cat->subcats as $scat)
                    {
        ?>{{$scat->name}}, <?php
                    }
                }
            }
        }
     ?>"/>
@stop

@section('assets')
    <link href="/css/nodes_list.css" rel="stylesheet">
@stop

@section('content')
<div class="page-header">
    <a href="{{url('shopmanager/addcat')}}" class="pull-right btn btn-primary btn-lg">
        <span class="glyphicon glyphicon-plus"></span> Добавить раздел
    </a>
    <h1>Разделы товаров</h1>
</div>
<div class="nodes-block list-nodes-block">
    @forelse($cats as $cat)
        <div class="node-item">
            <div class="list-group">
                  <div class="list-group-item">
                    <div class="cat-text">
                        <h4 class="list-group-item-heading">
                            {{$cat->name}}
                            <div class="pull-right">
                                <a href="{{url('shopmanager/editcat/'.$cat->slug)}}" class="btn btn-default btn-xs">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>
                                <a href="#" data-href="{{url('shopmanager/delcat/'.$cat->slug)}}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#deleteCat">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </a>
                            </div>
                        </h4>
                        <p class="list-group-item-text">{{$cat->desc}}</p>
                    </div>
                  </div>
                  
            
                  @if($cat->subcats)
                      <div id="{{$cat->slug}}" class="panel-collapse subcat-panel collapse in">
                        <ul class="list-group">
                            @foreach($cat->subcats as $sub)
                                <li class="list-group-item">
                                    <div class="pull-right">
                                        <a href="{{url('shopmanager/editcat/'.$sub->slug)}}" class="btn btn-default btn-xs">
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                        <a href="#" data-href="{{url('shopmanager/delcat/'.$sub->slug)}}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#deleteCat">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </a>
                                    </div>
                                    {{$sub->name}}
                                </li>
                            @endforeach
                        </ul>
                      </div>  
                  @endif
            </div>
        </div>
    @empty
        <p class="text-center">Категорий не найдено</p>
    @endforelse
    
    @include('shop.admin.modal.deleteCat')
</div>
@stop