@extends('shop.admin.index')

@section('assets')
    <script src="/js/jquery.synctranslit.min.js"></script>
@stop

@section('admin_content')
    <div class="page-header">
        <h1>Изменить характеристику</h1>
    </div>
    <form method="POST" action="{{url('shopmanager/editparam/'.$param->id)}}">
        {!! csrf_field(); !!}
        
        <div class="form-group">
            <label>Название характеристики</label>
            <input name="name" class="form-control" value="{{(old('name')) ? old('name') : $param->name}}" placeholder="Введите название характеристики"/>
        </div>
        <div class="form-group">
            <label>Техническое название</label>
            <input name="slug" id="paramslug" class="form-control" value="{{(old('slug')) ? old('slug') : $param->slug}}" placeholder="Будет сгенерировано автоматически"/>
        </div>
        <div class="form-group">
            <label>Единица измерения (отображается справа от значения характеристики в описании товара)</label>
            <input name="suffix" class="form-control" value="{{(old('suffix')) ? old('suffix') : $param->suffix}}" placeholder="Не более 25 символов"/>
        </div>
        
        <button type="submit" class="btn btn-lg btn-block btn-primary">Изменить характеристику</button>
    </form>
    
    <script>
        $(document).ready(function(){
            $('input[name=name]').syncTranslit({destination: 'paramslug'});
        });
    </script>
@stop