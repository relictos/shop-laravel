@extends('shop.admin.index')

@section('assets')
    <script src="/js/jquery.synctranslit.min.js"></script>
@stop

@section('admin_content')
    <div class="page-header">
        <h1>Добавить категорию (раздел) товаров</h1>
    </div>
    <form method="POST" action="{{url('shopmanager/addcat')}}">
        {!! csrf_field() !!}
        <div class="form-group">
            <label>Название категории</label>
            <input type="text" name="name" class="form-control" placeholder="Введите название категории товаров" value="{{old('name')}}"/>
        </div>
        <div class="form-group">
            <label>Ссылка на категорию</label>
            <input type="text" name="slug" id="slug" class="form-control" placeholder="Будет сгенерирована автоматически" value="{{old('slug')}}"/>
        </div>
        <script>
            $(document).ready(function(){
                $('input[name=name]').syncTranslit({destination: 'slug'});
            });
        </script>
        
        <div class="form-group">
            <label>Родительская категория</label>
            <select name="parent_cat" class="form-control">
                <option value="0">Без категории</option>
                @if($cats)
                    @foreach($cats as $cat)
                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                    @endforeach
                @endif
            </select>
            <p class="help-block">При выборе "без категории" ваша категория сама становится родительсткой. Это окончательное действие, которое невозможно изменить при редактировании.</p>
        </div>
        
        <div class="form-group">
            <label>Описание категории (не более 255 символов)</label>
            <textarea class="form-control" name="desc">{{old('desc')}}</textarea>
        </div>
        
        <button type="submit" class="btn btn-primary btn-lg btn-block">Создать категорию</button>
    </form>
@stop