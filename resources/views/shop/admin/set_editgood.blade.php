@extends('shop.admin.index')

@section('admin_content')
    <div class="page-header">
        <h2>Изменить товар в наборе</h2>
    </div>
    <form method="POST" action="{{url('shopmanager/seteditgood/'.$set->slug.'/'.$good->id)}}">
        {{csrf_field()}}
        <div class="form-group">
            <label>Товар</label>
            <select name="good_id" class="form-control">
                @foreach($goods as $sgood)
                    <option value="{{$sgood->id}}">{{$sgood->name}}</option>
                @endforeach
            </select>
            <script>
                $('[name=good_id]').val({{(old('good_id')) ? old('good_id') : $good->good->id}});
            </script>
        </div>
        <div class="form-group">
            <label>Количество</label>
            <input class="form-control" name="count" value="{{(old('count')) ? old('count') : $good->count}}" placeholder="Количество выбранных товаров в наборе"/>
        </div>
        
        <button type="submit" class="btn btn-primary btn-block btn-lg">Изменить товар в наборе</button>
    </form>
@stop