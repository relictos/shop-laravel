@extends('shop.admin.index')

@section('admin_content')
    <div class="page-header">
        <h1>Изменить характеристики</h1>
    </div>
    <form method="POST" action="{{url('shopmanager/editgoodparam/'.$good->slug.'/'.$param->id)}}">
        {!! csrf_field(); !!}
        
        <div class="form-group">
            <div class="pull-right">
                <a href="#" data-href="{{url('shopmanager/createparam')}}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#createParam">
                    <span class="glyphicon glyphicon-plus"></span> Создать новую характеристику
                </a>
                <a href="{{url('shopmanager/params')}}" target="_blank" class="btn btn-default btn-xs">Список характеристик</a>
            </div>
            <label>Характеристика</label>
            <select class="form-control" name="param">
                @foreach($params as $sparam)
                    <option value="{{$sparam->id}}">{{$sparam->name}} ({{$sparam->suffix}})</option>
                @endforeach    
            </select>
            <script>
                $('[name="param"]').val("{{$param->param_id or 0}}");
            </script>
        </div>
        
        <div class="form-group">
            <label>Значение</label>
            <input type="text" class="form-control" name="value" value="{{(old('value')) ? old('value') : $param->value}}" />
        </div>
        
        <button type="submit" class="btn btn-lg btn-block btn-primary">Изменить характеристику</button>
    </form>
    
    @include('shop.admin.modal.createParam')
@stop