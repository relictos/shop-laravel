<div class="modal fade" id="deleteParam" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Подтверждение удаления</h4>
      </div>
      <div class="modal-body">
        Вы уверены, что хотите удалить эту характеристику?    
      </div>
      <div class="modal-footer">
        <form method="POST" class="delform" action="">
            <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            {!! csrf_field() !!}
            <button type="submit" class="btn btn-primary">Удалить</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
    $('#deleteParam').on('show.bs.modal', function(e) {
        $(this).find('.delform').attr('action', $(e.relatedTarget).data('href'));
    });
</script>