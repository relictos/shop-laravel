<script src="/js/jquery.synctranslit.min.js"></script>

<div class="modal fade" id="createParam" tabindex="-1" role="dialog">
    <form method="POST" class="createform" action="">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Создание характеристики</h4>
          </div>
          <div class="modal-body">
                <div class="form-group">
                    <label>Название характеристики</label>
                    <input name="name" class="form-control" value="{{old('name')}}" placeholder="Введите название характеристики"/>
                </div>
                <div class="form-group">
                    <label>Техническое название</label>
                    <input name="slug" id="paramslug" class="form-control" value="{{old('slug')}}" placeholder="Будет сгенерировано автоматически"/>
                </div>
                <div class="form-group">
                    <label>Единица измерения (отображается справа от значения характеристики в описании товара)</label>
                    <input name="suffix" class="form-control" value="{{old('suffix')}}" placeholder="Не более 25 символов"/>
                </div>
          </div>
          <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                {!! csrf_field() !!}
                <button type="submit" class="btn btn-primary">Создать характеристику</button>
          </div>
        </div>
      </div>
    </form>
</div>

<script>
    $('#createParam').on('show.bs.modal', function(e) {
        $(this).find('.createform').attr('action', $(e.relatedTarget).data('href'));
    });
</script>
<script>
    $(document).ready(function(){
        $('.createform input[name=name]').syncTranslit({destination: 'paramslug'});
    });
</script>