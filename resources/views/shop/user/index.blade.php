@extends('index')

@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a class="pull-right btn btn-default btn-xs" href="{{url('auth/logout')}}">
                    <span class="glyphicon glyphicon-log-out"></span>
                </a>
                {{$user->email}}
            </div>
            <div class="list-group">
                @section('user_menu')
                    <a class="list-group-item active" href="#">Обо мне</a>
                    <a class="list-group-item" href="{{url('profile/orders')}}">Мои заказы</a>
                @show
                
                @if(Auth::user()->role == 'admin')
                    <a class="list-group-item" href="{{url('shopmanager')}}">Панель администратора</a>
                @endif
            </div>
        </div>    
    </div>
    <div class="col-md-9">
        @yield('user_content')
    </div>
</div>
@stop