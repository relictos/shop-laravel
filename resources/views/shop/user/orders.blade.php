@extends('shop.user.index')

@section('title')
    <title>Заказы</title>
    <meta name="description" content="Список заказов"/>
@stop

@section('user_menu')
    <a class="list-group-item" href="{{url('profile')}}">Обо мне</a>
    <a class="list-group-item active" href="#">Мои заказы</a>
@stop

@section('user_content')
    <div class="page-header">
        <h2>Мои заказы</h2>
    </div>
    
    @forelse($orders as $order)
        <div class="panel panel-default order-panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        
                        <a class="vidget-title"><span class="caret"></span> Заказ #{{$order->id}}</a>
                        ({{$order->created_at->format('d.m.Y')}})
                    </div>
                    <div class="col-md-3"><strong>{{$order->price}}р.</strong></div>
                    <div class="col-md-2">
                        <span class="{{$order->state->style}}">{{$order->state->name}}</span>
                    </div>
                    <div class="col-md-3">
                        <a class="btn btn-info btn-xs btn-block" href="#order{{$order->id}}" data-toggle="collapse">Показать</a>
                    </div>
                </div>
            </div>
            <div class="list-group collapse" id="order{{$order->id}}">
                @forelse($order->goods as $good)
                    <div class="list-group-item">
                        <div class="list-group-item-text">
                            <div class="row">
                                <div class="col-sm-1 col-xs-3">
                                    <img src="/img/{{$good->info->image}}" width="100%"/>
                                </div>
                                <div class="col-xs-9 col-sm-7">
                                    <h4 class="list-group-item-heading">
                                        <a href="{!! route($good->type,$good->info->slug) !!}" target="_blank">{{$good->info->name}}</a>
                                    </h3>
                                    <p class="help-block">{{$good->info->price}}р.</p>
                                </div>
                                <div class="col-sm-2">
                                    <span>Количество:</span>
                                    <p><b>{{$good->count}} шт.</b></p>
                                </div>
                                <div class="col-sm-2">
                                    <span>Стоимость:</span>
                                    <p><b>{{$good->info->price*$good->count}}р.</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <p class="text-center">Товаров не найдено</p>
                @endforelse
            </div>
        </div>
    @empty
        <p class="text-center">У вас еще нет заказов</p>
    @endforelse

@stop