@extends('shop.user.index')

@section('title')
    <title>Личный кабинет</title>
    <meta name="description" content="личный кабинет пользователя"/>
@stop

@section('user_menu')
    <a class="list-group-item active" href="#">Обо мне</a>
    <a class="list-group-item" href="{{url('profile/orders')}}">Мои заказы</a>
@stop

@section('user_content')
<div class="page-header">
    <h2>
        <a href="{{url('profile/edit')}}" class="btn btn-default pull-right">Редактировать <span class="glyphicon glyphicon-edit"></span></a>
        Обо мне
    </h2>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <h3 class="vidget-title">Основные данные</h3>
        <ul class="list-unstyled info-list">
            <li>ФИО: <span class="info-text">{{$user->social->fio or 'не указано'}}</span></li>
            @if($user->social)
            <li>Пол: <span class="info-text">{{($user->social->gender == 'male') ? 'Мужчина' : 'Женщина'}}</span></li>
            @endif
            <li>Email: <span class="info-text">{{$user->email}}</span></li>
            <li>Город: <span class="info-text">Екатеринбург</span></li>
            <li>Телефон: <span class="info-text">{{$user->social->phone or 'не указан'}}</span></li>
        </ul>                        
    </div>
</div>
@stop