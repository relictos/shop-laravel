@extends('index')

@section('assets')
    <script src="/js/jquery.maskedinput.js"></script>
@stop

@section('content')
    <div class="page-header">
        <h1>Редактировать личные данные</h1>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form method="POST" action="{{url('profile/edit')}}">
                {!! csrf_field() !!}
                
                <div class="form-group">
                    <label>Фамилия</label>
                    <input class="form-control" type="text" name="fio_sname" value="{{$user->social->fio_arr[0] or ''}}">
                </div>
                <div class="form-group">
                    <label>Имя</label>
                    <input class="form-control" type="text" name="fio_name" value="{{$user->social->fio_arr[1] or ''}}">
                </div>
                <div class="form-group">
                    <label>Отчество</label>
                    <input class="form-control" type="text" name="fio_fname" value="{{$user->social->fio_arr[2] or ''}}">
                </div>
                
                <div class="form-group">
                    <label for="phone">Номер телефона</label>
                    <input name="phone" type="text" class="form-control" id="phone" placeholder="Введите номер телефона" value="{{$user->social->phone or ''}}">
                    <script> $("#phone").mask("+7(999) 999-9999");</script>
                </div>
                
                <button type="submit" class="btn btn-primary btn-block">Изменить</button>
            </form>
        </div>
    </div>
@stop