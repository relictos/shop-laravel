(function( $ ){

  $.fn.imgAdaptiveList = function() {
    var imgBlock = this;
    var fullwidth = 0;
    var blockwidth = $(this).width(); 
        
    $(document).waitForImages(function(){ 
        
        $('#'+$(imgBlock).attr('id')+' .image-item img').each(function(){
            fullwidth += $(this).width();
        });
    
        width_percent = fullwidth / blockwidth;
        fullwidth += 22*width_percent;
        
        $('#'+$(imgBlock).attr('id')+' .image-item img').each(function(){
            var percent = $(this).width() / fullwidth * 100;
            var wd = $(imgBlock).width() * percent -4;
            $(this).parent().width((percent)+'%')
        });
    });
  };
})( jQuery );