var bought_text = '<span class="glyphicon glyphicon-ok"></span>'

$(document).ready(function()
{   
    $('.btn-buy').click(function(){
        if(!$(this).hasClass('btn-buy')) return;
        
        var type = $(this).data('type');
        var slug = $(this).data('slug');
        
        var cart = Cookies.get('cart');
        
        if(!cart) cart = [];
        else cart = JSON.parse(cart);
        
        var good_exists = false;
        
        for(i = 0; i < cart.length; i++)
        {
            if((cart[i].type == type) && (cart[i].slug == slug))
            {
                good_exists = true;
                cart[i].count += 1;
                break;
            }
        }
        
        if(!good_exists)
            cart.push({'type':type,'slug':slug,'count':1});
        
        Cookies.set('cart',JSON.stringify(cart));
        cart_count();
        
        $(this).removeClass('btn-buy').removeClass('btn-primary').addClass('btn-default').html(bought_text);
    });
    
    cart_count();
});

function cart_count()
{
    var cart = Cookies.get('cart');
    var scount = 0;
    
    if(!cart) return $('#cartcount').html(scount);
    else cart = JSON.parse(cart);
    
    for(i=0; i< cart.length; i++)
    {
        scount += cart[i].count;
    }
    
    $('#cartcount').html(scount);
    return scount;
}