$(document).ready(function() {

  var sync1 = $("#sync1");

  sync1.owlCarousel({
	autoPlay: 8000,
	singleItem : true,
	slideSpeed : 500,
	//afterAction : syncPosition,
	responsiveRefreshRate : 200,
    autoHeight : true,
    afterInit : function(elem){
      var that = this
      that.owlControls.prependTo(elem)
    }
  });
  
  });