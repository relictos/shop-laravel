var itemdeleted = '<p class="text-center">Товар удален из корзины</p>';

$(document).ready(function(){
    $('.del-item').click(function(){
       var type = $(this).data('type');
       var slug = $(this).data('slug');
       
       if(cart_delete(type,slug))
       {
            $(this).parent().html(itemdeleted);
       }
    });
    
    $('.btn-cartplus').click(function(){
        var type = $(this).data('type');
        var slug = $(this).data('slug');
        cart_changecount(type,slug,1);
    });
    
    $('.btn-cartminus').click(function(){
        var type = $(this).data('type');
        var slug = $(this).data('slug');
        cart_changecount(type,slug,-1);
    });
    
    cart_price();
})

function cart_delete(type,slug)
{
    var cart = Cookies.get('cart');
    
    if(!cart) cart = [];
    else cart = JSON.parse(cart);
    
    for(i = 0; i < cart.length; i++)
    {
        if((cart[i].type == type) && (cart[i].slug == slug))
        {
            cart.splice(i,1);
            Cookies.set('cart',JSON.stringify(cart));
            cart_count();
            cart_price();
            return true;
        }
    }  
    
    return false;  
}

function cart_price()
{
    var cart = Cookies.get('cart');
    var price = 0;
    
    if(!cart) cart = [];
    else cart = JSON.parse(cart);
    
    for(i = 0; i < cart.length; i++)
    {
        sprice = $('.'+cart[i].type+'-'+cart[i].slug+'-price').data('singleprice');
        if(!sprice) sprice = 0;
        
        price += sprice*cart[i].count;
    }  
    
    $('#cartprice').html(price);
    return price;      
}

function cart_itemprice(type,slug)
{
    var price = 0;
    var singleprice = $('.'+type+'-'+slug+'-price').data('singleprice');
    var count = $('.'+type+'-'+slug+'-count').val();
    
    price = singleprice*count;
    $('.'+type+'-'+slug+'-price').html(price);
}

function cart_changecount(type,slug,scount)
{
    var cart = Cookies.get('cart');
    
    if(!cart) cart = [];
    else cart = JSON.parse(cart);
    
    for(i = 0; i < cart.length; i++)
    {
        if((cart[i].type == type) && (cart[i].slug == slug))
        {
            cart[i].count += scount;
            if(cart[i].count == 0)
            {
                cart[i].count = 1;
            }
            
            $('.'+type+'-'+slug+'-count').val(cart[i].count);
            cart_itemprice(type,slug);
            
            Cookies.set('cart',JSON.stringify(cart));
            cart_count();
            cart_price();
        
            return true;
        }
    }  
    
    return false;     
}