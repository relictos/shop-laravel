$(document).ready(function(){
    var data = {
        labels: ["1", "5", "10", "15", "20", "25", "30"],
        datasets: [
            {
                label: '������ 2015',
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [15, 13, 25, 14, 17, 28, 34]
            }
        ]
    };
    var ctx = document.getElementById("ordersChart").getContext("2d");
    var ordersChart = new Chart(ctx).Line(data, {
       responsive : true,
       animation: true,
       barValueSpacing : 5,
       barDatasetSpacing : 1,
       tooltipFillColor: "rgba(0,0,0,0.8)",                
       tooltipTemplate: "<%= label %> <%= datasetLabel %> - <%= value %> �������"
    });
    
    var paydata = {
        labels: ["1", "5", "10", "15", "20", "25", "30"],
        datasets: [
            {
                label: '������ 2015',
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [35000, 22000, 65000, 14900, 65200, 92800, 157000]
            }
        ]
    };
    var ctx2 = document.getElementById("payChart").getContext("2d");
    var paymentsChart = new Chart(ctx2).Line(paydata, {
       responsive : true,
       animation: true,
       barValueSpacing : 5,
       barDatasetSpacing : 1,
       tooltipFillColor: "rgba(0,0,0,0.8)",                
       tooltipTemplate: "<%= label %> <%= datasetLabel %> - <%= value %> ������"
    });    

    $('#graph_payments').removeClass('active');                        
});
